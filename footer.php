<?php
/**
 **/
do_action( 'hemelios_main_wrapper_content_end' );
?>
<div class="footer_image" style="height:143px;">

</div>
</div>
<!-- Close Wrapper Content -->

<?php
$hemelios_options = hemelios_option();
$main_footer_class = array( 'main-footer-wrapper' );
if ( isset( $hemelios_options['footer_parallax'] ) && $hemelios_options['footer_parallax'] == '1' ) {
	$main_footer_class[] = 'enable-parallax';
}

if ( $hemelios_options['collapse_footer'] == '1' ) {
	$main_footer_class[] = 'footer-collapse-able';
}


// SHOW FOOTER
$prefix           = 'hemelios_';
$footer_show_hide = hemelios_get_post_meta_box_option( $prefix . 'footer_show_hide' );
if ( ( $footer_show_hide === '' ) ) {
	$footer_show_hide = '1';
}


?>
<?php if ( $footer_show_hide == '1' ): ?>
	<footer class="<?php echo join( ' ', $main_footer_class ) ?>">
		<div class="half_sin_wrapper"><canvas class="half_sin" data-bg-color="23,108,129" data-line-color="23,108,129" height="10" width="1920" style="height: 10px; width: 1920px;"></canvas></div>
		<div id="wrapper-footer">
			<?php
			/**
			 * @hooked - hemelios_footer_widgets - 10
			 * @hooked - hemelios_bottom_bar_filter - 20
			 *
			 **/
			do_action( 'hemelios_main_wrapper_footer' );
			?>
		</div>
	</footer>
<?php endif; ?>
</div>
<!-- Close Wrapper -->

<?php
/**
 * @hooked - hemelios_back_to_top - 5
 **/
do_action( 'hemelios_after_page_wrapper' );
?>

<?php wp_footer(); ?>
<div id="login_popup_wrapper" class="dialog">
	<div class="dialog__overlay"></div>
	<div class="dialog__content">
		<div class="morph-shape">
			<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 520 280"
				 preserveAspectRatio="none">
				<rect x="3" y="3" fill="none" width="516" height="276" />
			</svg>
		</div>
		<div class="dialog-inner">
			<h2><?php echo esc_html__( 'Đăng nhập vào hệ thống', 'hemelios' ); ?></h2>

			<?php
			if( function_exists('login_with_ajax') ) {
				login_with_ajax();
			}
			?>
			<div>
				<button class="action" data-dialog-close="close" type="button"><i class="fa fa-close"></i></button>
			</div>
		</div>
	</div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1710201865863501";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html> <!-- end of site. what a ride! -->