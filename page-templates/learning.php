<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 4/25/2016
 * Time: 5:32 PM
 */
//Template name: Bài tập về nhà

get_header();

$hemelios_options = hemelios_option();
$user_ID          = get_current_user_id();
$page_level       = hemelios_get_post_meta_box_option( 'hemelios_page_level' );
$user_level       = get_the_author_meta( 'member_level', $user_ID );
$expiration_date  = get_the_author_meta( 'expiration_date', $user_ID );
$now              = date( 'd-m-Y' );

$layout_style = hemelios_get_post_meta( $post->ID, 'hemelios_page_layout', true );
if ( ( $layout_style === '' ) || ( $layout_style == '-1' ) ) {
	$layout_style = $hemelios_options['page_layout'];
}

$sidebar = hemelios_get_post_meta( $post->ID, 'hemelios_page_sidebar', true );
if ( ( $sidebar === '' ) || ( $sidebar == '-1' ) ) {
	$sidebar = $hemelios_options['page_sidebar'];
}

$left_sidebar = hemelios_get_post_meta( $post->ID, 'hemelios_page_left_sidebar', true );
if ( ( $left_sidebar === '' ) || ( $left_sidebar == '-1' ) ) {
	$left_sidebar = $hemelios_options['page_left_sidebar'];

}

$right_sidebar = hemelios_get_post_meta( $post->ID, 'hemelios_page_right_sidebar', true );
if ( ( $right_sidebar === '' ) || ( $right_sidebar == '-1' ) ) {
	$right_sidebar = $hemelios_options['page_right_sidebar'];
}

$sidebar_width = hemelios_get_post_meta( $post->ID, 'hemelios_sidebar_width', true );
if ( ( $sidebar_width === '' ) || ( $sidebar_width == '-1' ) ) {
	$sidebar_width = $hemelios_options['page_sidebar_width'];
}


// Calculate sidebar column & content column
$sidebar_col = 'col-md-3';
if ( $sidebar_width == 'large' ) {
	$sidebar_col = 'col-md-4';
}

$content_col_number = 12;
if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'left' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}
if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'right' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}

$content_col = 'col-md-' . $content_col_number;
if ( ( $content_col_number == 12 ) && ( $layout_style == 'full' ) ) {
	$content_col = '';
}


$page_content_mb = hemelios_get_post_meta_box_option( 'hemelios_page_content_mb' );

if ( $page_content_mb == '' || $page_content_mb == '-1' ) {
	if ( isset( $hemelios_options['page_content_mb'] ) ) {
		$page_content_mb = $hemelios_options['page_content_mb'];
	}
}

// Page sidebar bottom
$show_page_bottom_sidebar = hemelios_get_post_meta_box_option( 'hemelios_show_page_bottom_sidebar' );
$page_bottom_sidebar      = hemelios_get_post_meta_box_option( 'hemelios_page_bottom_sidebar' );


if ( $show_page_bottom_sidebar == '' || $show_page_bottom_sidebar == '-1' ) {
	if ( isset( $hemelios_options['page_bottom_sidebar'] ) ) {
		$page_bottom_sidebar = $hemelios_options['page_bottom_sidebar'];
	} else {
		$page_bottom_sidebar = '';
	}
}


// Main Class
$main_class = array( 'site-content-page' );

if ( $content_col_number < 12 ) {
	$main_class[] = 'has-sidebar';
}

if ( $page_content_mb == '1' ) {
	$main_class[] = 'mb-bottom';
} elseif ( $page_content_mb == '0' ) {
	$main_class[] = 'no-mb';
}

?>
<?php
/**
 * @hooked - hemelios_page_heading - 5
 **/
do_action( 'hemelios_before_page' );
?>
	<main class="<?php echo join( ' ', $main_class ) ?>">
		<?php if ( $layout_style != 'full' ): ?>
		<div class="<?php echo esc_attr( $layout_style ) ?> clearfix">
			<?php endif; ?>

			<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
			<div class="row clearfix">
				<?php endif; ?>

				<?php if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'left' ) || ( $sidebar == 'both' ) ) ): ?>
					<div class="sidebar left-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
						<?php dynamic_sidebar( $left_sidebar ); ?>
					</div>
				<?php endif; ?>
				<div class="site-content-page-inner <?php echo esc_attr( $content_col ) ?>">
					<div class="page-content">
						<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();
							// Include the page content template.
							if( is_user_logged_in() && is_super_admin( $userID ) ){
								hemelios_get_template( 'content', 'page' );
							}else{
								if ( !is_user_logged_in() ) {
									?>
									<div class="os-notification style2 ">
										<div class="alert fade in alert-error">
											<i class="fa fa-times-circle"></i>
											<div class="alert-content">
												<strong>Bạn không có quyền truy cập trang này</strong>
												Vui lòng <a id="login-link" href="javascript:;">Đăng nhập </a> để sử dụng
											</div>
										</div>
									</div>
									<?php
								} elseif ( strtotime( $now ) >= strtotime( $expiration_date ) ) {
									?>
									<div class="os-notification style2 ">
										<div class="alert fade in alert-error">
											<i class="fa fa-times-circle"></i>
											<div class="alert-content">
												<strong>Tài khoản của bạn đã hết hạn</strong>
												Vui lòng liên hệ với quản trị viên để gia hạn tài khoản
											</div>
										</div>
									</div>
									<?php
								} elseif ( $page_level == $user_level ) {
									hemelios_get_template( 'content', 'page' );
								} else { ?>
									<div class="os-notification style2 ">
										<div class="alert fade in alert-error">
											<i class="fa fa-times-circle"></i>
											<div class="alert-content">
												<strong>Tài khoản của bạn không có quyền truy cập trang này</strong>
												Vui lòng liên hệ với quản trị viên để được giúp đỡ
											</div>
										</div>
									</div>
									<?php
								}
							}

						endwhile;
						?>
					</div>
				</div>
				<?php if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'right' ) || ( $sidebar == 'both' ) ) ): ?>

					<div class="sidebar right-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
						<?php dynamic_sidebar( $right_sidebar ); ?>
					</div>
				<?php endif; ?>
				<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
			</div>
		<?php endif; ?>

			<?php if ( $layout_style != 'full' ): ?>
		</div>
	<?php endif; ?>
	</main>
<?php if ( $show_page_bottom_sidebar != '0' ): ?>
	<?php if ( isset( $page_bottom_sidebar ) && $page_bottom_sidebar != '' ) ?>
	<?php if ( is_active_sidebar( $page_bottom_sidebar ) ): ?>
		<div class="bottom-page sidebar">
			<?php dynamic_sidebar( $page_bottom_sidebar ); ?>
		</div>
	<?php endif; ?>
<?php endif; ?>


<?php get_footer(); ?>