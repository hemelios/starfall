<?php
/**
 * Template Name: Coming Soon
 *
 * @package hemelios
 */

remove_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_above_header', 10 );
remove_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_top_bar', 10 );
remove_action( 'hemelios_before_page_wrapper_content', 'hemelios_page_header', 15 );

get_header();
?>
<?php echo do_shortcode( '[hemelios_countdown_shortcode type="comming-soon"]' ); ?>
<?php
remove_action( 'hemelios_main_wrapper_footer', 'hemelios_footer_widgets', 10 );
get_footer();
?>