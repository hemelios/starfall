<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 5/11/2016
 * Time: 6:55 AM
 */

//Template Name: Kiểm tra trình độ


get_header();

$hemelios_options = hemelios_option();
$user_ID          = get_current_user_id();
$page_level       = hemelios_get_post_meta_box_option( 'hemelios_page_level' );
$user_level       = get_the_author_meta( 'member_level', $user_ID );
$expiration_date  = get_the_author_meta( 'expiration_date', $user_ID );
$now              = date( 'd-m-Y' );

$layout_style = hemelios_get_post_meta( $post->ID, 'hemelios_page_layout', true );
if ( ( $layout_style === '' ) || ( $layout_style == '-1' ) ) {
	$layout_style = $hemelios_options['page_layout'];
}

$sidebar = hemelios_get_post_meta( $post->ID, 'hemelios_page_sidebar', true );
if ( ( $sidebar === '' ) || ( $sidebar == '-1' ) ) {
	$sidebar = $hemelios_options['page_sidebar'];
}

$left_sidebar = hemelios_get_post_meta( $post->ID, 'hemelios_page_left_sidebar', true );
if ( ( $left_sidebar === '' ) || ( $left_sidebar == '-1' ) ) {
	$left_sidebar = $hemelios_options['page_left_sidebar'];

}

$right_sidebar = hemelios_get_post_meta( $post->ID, 'hemelios_page_right_sidebar', true );
if ( ( $right_sidebar === '' ) || ( $right_sidebar == '-1' ) ) {
	$right_sidebar = $hemelios_options['page_right_sidebar'];
}

$sidebar_width = hemelios_get_post_meta( $post->ID, 'hemelios_sidebar_width', true );
if ( ( $sidebar_width === '' ) || ( $sidebar_width == '-1' ) ) {
	$sidebar_width = $hemelios_options['page_sidebar_width'];
}


// Calculate sidebar column & content column
$sidebar_col = 'col-md-3';
if ( $sidebar_width == 'large' ) {
	$sidebar_col = 'col-md-4';
}

$content_col_number = 12;
if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'left' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}
if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'both' ) || ( $sidebar == 'right' ) ) ) {
	if ( $sidebar_width == 'large' ) {
		$content_col_number -= 4;
	} else {
		$content_col_number -= 3;
	}
}

$content_col = 'col-md-' . $content_col_number;
if ( ( $content_col_number == 12 ) && ( $layout_style == 'full' ) ) {
	$content_col = '';
}


$page_content_mb = hemelios_get_post_meta_box_option( 'hemelios_page_content_mb' );

if ( $page_content_mb == '' || $page_content_mb == '-1' ) {
	if ( isset( $hemelios_options['page_content_mb'] ) ) {
		$page_content_mb = $hemelios_options['page_content_mb'];
	}
}

// Page sidebar bottom
$show_page_bottom_sidebar = hemelios_get_post_meta_box_option( 'hemelios_show_page_bottom_sidebar' );
$page_bottom_sidebar      = hemelios_get_post_meta_box_option( 'hemelios_page_bottom_sidebar' );


if ( $show_page_bottom_sidebar == '' || $show_page_bottom_sidebar == '-1' ) {
	if ( isset( $hemelios_options['page_bottom_sidebar'] ) ) {
		$page_bottom_sidebar = $hemelios_options['page_bottom_sidebar'];
	} else {
		$page_bottom_sidebar = '';
	}
}


// Main Class
$main_class = array( 'site-content-page' );

if ( $content_col_number < 12 ) {
	$main_class[] = 'has-sidebar';
}

if ( $page_content_mb == '1' ) {
	$main_class[] = 'mb-bottom';
} elseif ( $page_content_mb == '0' ) {
	$main_class[] = 'no-mb';
}

?>
<?php
/**
 * @hooked - hemelios_page_heading - 5
 **/
do_action( 'hemelios_before_page' );
?>
	<main class="<?php echo join( ' ', $main_class ) ?>">
		<?php if ( $layout_style != 'full' ): ?>
		<div class="<?php echo esc_attr( $layout_style ) ?> clearfix">
			<?php endif; ?>

			<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
			<div class="row clearfix">
				<?php endif; ?>

				<?php if ( is_active_sidebar( $left_sidebar ) && ( ( $sidebar == 'left' ) || ( $sidebar == 'both' ) ) ): ?>
					<div class="sidebar left-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
						<?php dynamic_sidebar( $left_sidebar ); ?>
					</div>
				<?php endif; ?>
				<div class="site-content-page-inner <?php echo esc_attr( $content_col ) ?>">
					<div class="page-content entry-content">
						<?php  while ( have_posts() ) : the_post(); ?>
							<?php hemelios_get_template( 'content', 'page' ); ?>
						<?php endwhile; ?>
						<div class="check-level-wrapper">
							<?php
							$level        = $hemelios_options['level_list'];
							$select_level = '';
							$selected     = '';
							if ( $level ) :
								$select_level .= '<select class="regular-text" id="member_level" name="member_level">';
								$display= array('5 - 6 Tuổi', '6 - 8 Tuổi', '8 - 10 Tuổi', '10 - 14 Tuổi');
								foreach ( $level as $key => $value ) {
									$selected = $user_level == sanitize_title( $value ) ? 'selected' : '';
									$page_id = get_post_meta(get_the_ID(),'hemelios_'.sanitize_title( $value ).'',true);
									$option_value = get_page_link($page_id);
									$select_level .= '<option value="' . $option_value . '" ' . $selected . '>' . $display[$key] . '</option>';
								}
								$select_level .= '</select>';
							endif;
							?>
							<form action="" id="check-level-form">
								<div class="row">
									<div class="col-md-6 col-xs-12 col-md-offset-3">
										<div class="input-wrapper">
											<label for="member_level">Chọn độ tuổi cho bé <span style="color: #f00">*</span></label>
											<?php echo $select_level ?>
										</div>

										<div class="input-wrapper">
											<label for="you_name">Họ tên phụ huynh <span style="color: #f00">*</span></label>
											<input type="text" name="you_name" id="you_name" placeholder="Họ tên phụ huynh" required />
										</div>

										<div class="input-wrapper">
											<label for="you_phone">Điện thoại liên hệ <span style="color: #f00">*</span></label>
											<input type="number" name="you_phone" id="you_phone" placeholder="Số điện thoại liên hệ" required />
										</div>
										<div class="input-wrapper">
											<label for="you_email">Email liên hệ</label>
											<input type="email" name="you_email" id="you_email" placeholder="Email phụ huynh" />
										</div>
										<div class="input-wrapper">
											<label for="child_name">Họ tên trẻ <span style="color: #f00">*</span></label>
											<input type="email" name="child_name" id="child_name" placeholder="Họ tên trẻ" />
										</div>
										<div class="input-wrapper">
											<label for="child_date">Năm sinh của trẻ</label>
											<input type="email" name="child_date" id="child_date" placeholder="Năm sinh của trẻ" />
										</div>
										<div class="input-wrapper">
											<label for="you_message">Lời nhắn cho Starfall</label>
											<textarea name="you_message" id="you_message" cols="30" rows="2" placeholder="Nhập nội dung tin nhắn"></textarea>
										</div>
										<div class="input-wrapper text-right">
											<input type="submit" name="sumit_check_level" id="sumit_check_level" value="Bắt đầu kiểm tra" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<?php if ( is_active_sidebar( $right_sidebar ) && ( ( $sidebar == 'right' ) || ( $sidebar == 'both' ) ) ): ?>

					<div class="sidebar right-sidebar <?php echo esc_attr( $sidebar_col ) ?>">
						<?php dynamic_sidebar( $right_sidebar ); ?>
					</div>
				<?php endif; ?>
				<?php if ( ( $content_col_number != 12 ) || ( $layout_style != 'full' ) ): ?>
			</div>
		<?php endif; ?>

			<?php if ( $layout_style != 'full' ): ?>
		</div>
	<?php endif; ?>
	</main>
<?php if ( $show_page_bottom_sidebar != '0' ): ?>
	<?php if ( isset( $page_bottom_sidebar ) && $page_bottom_sidebar != '' ) ?>
	<?php if ( is_active_sidebar( $page_bottom_sidebar ) ): ?>
		<div class="bottom-page sidebar">
			<?php dynamic_sidebar( $page_bottom_sidebar ); ?>
		</div>
	<?php endif; ?>
<?php endif; ?>


<?php get_footer(); ?>