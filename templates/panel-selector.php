<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/20/2015
 * Time: 8:56 AM
 */
?>
<?php
$style_selector_bg_url =  get_template_directory_uri()  . '/assets/images/style-selector-bg.png';
?>
<div id="panel-style-selector">
	<div class="panel-wrapper">
		<div class="panel-selector-open"><i class="fa fa-cog"></i></div>
		<div class="panel-selector-header"><?php echo esc_html__( 'Style Selector', 'hemelios' ); ?></div>
		<div class="panel-selector-body clearfix">
			<section class="panel-selector-section color-section clearfix">
				<h3 class="panel-selector-title"><?php echo esc_html__( 'Primary Color', 'hemelios' ); ?></h3>

				<div class="panel-selector-row clearfix">
					<ul class="panel-primary-color">
						<li style="background-color: #0cb4ce" data-color="#0cb4ce"></li>
						<li style="background-color: #c97178" data-color="#c97178"></li>
						<li style="background-color: #6b58cd" data-color="#6b58cd"></li>
						<li style="background-color: #ffb600" data-color="#ffb600"></li>
						<li style="background-color: #af1d32" data-color="#af1d32"></li>
						<li style="background-color: #205f5e" data-color="#205f5e"></li>
						<li style="background-color: #4f9f37" data-color="#4f9f37"></li>
						<li style="background-color: #866730" data-color="#866730"></li>
					</ul>
				</div>
			</section>

			<section class="panel-selector-section layout-section clearfix">
				<h3 class="panel-selector-title"><?php echo esc_html__( 'Layout', 'hemelios' ); ?></h3>

				<div class="panel-selector-row clearfix">
					<a data-type="layout" data-value="wide" href="#" class="panel-selector-btn"><?php echo esc_html__( 'Wide', 'hemelios' ); ?></a>
					<a data-type="layout" data-value="boxed" href="#" class="panel-selector-btn"><?php echo esc_html__( 'Boxed', 'hemelios' ); ?></a>
				</div>
			</section>
			<section class="panel-selector-section background-section clearfix">
				<h3 class="panel-selector-title"><?php echo esc_html__( 'Background', 'hemelios' ); ?></h3>


				<div class="panel-selector-row clearfix">
					<ul class="panel-primary-background">
						<li class="pattern-0" data-name="pattern-1.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: 0px 0px;"></li>
						<li class="pattern-1" data-name="pattern-2.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -45px 0px;"></li>
						<li class="pattern-2" data-name="pattern-3.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -90px 0px;"></li>
						<li class="pattern-3" data-name="pattern-4.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -135px 0px;"></li>
						<li class="pattern-4" data-name="pattern-5.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -180px 0px;"></li>
						<li class="pattern-5" data-name="pattern-6.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -225px 0px;"></li>
						<li class="pattern-6" data-name="pattern-7.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -270px 0px;"></li>
						<li class="pattern-7" data-name="pattern-8.png" data-type="pattern" style="background-image:  url(<?php echo esc_html($style_selector_bg_url); ?>); background-position: -315px 0px;"></li>
					</ul>
				</div>
			</section>
			<section class="panel-selector-section rtl-section clearfix">
				<h3 class="panel-selector-title"><?php echo esc_html__( 'RTL Mode', 'hemelios' ); ?></h3>

				<div class="panel-selector-row clearfix">
					<a id="panel-selector-reset" href="#" class="panel-selector-btn"><?php echo esc_html__( 'Reset', 'hemelios' ); ?></a>
					<a data-mode="off" id="panel-selector-rtl" href="#" class="panel-selector-btn"><?php echo esc_html__( 'RTL On', 'hemelios' ); ?></a>
				</div>
			</section>
		</div>
	</div>
</div>