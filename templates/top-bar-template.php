<?php
$hemelios_options = hemelios_option();
$prefix          = 'hemelios_';
$is_show_top_bar = hemelios_get_post_meta_box_option( $prefix . 'top_bar' );
if ( ( $is_show_top_bar === '' ) || ( $is_show_top_bar == '-1' ) ) {
	$is_show_top_bar = $hemelios_options['top_bar'];
}
if ( !$is_show_top_bar ) {
	return; // NOT SHOW TOP BAR
}
$top_bar_layout = hemelios_get_post_meta_box_option( $prefix . 'top_bar_layout' );
if ( ( $top_bar_layout == '' ) || ( $top_bar_layout == '-1' ) ) {
	$top_bar_layout = $hemelios_options['top_bar_layout'];
}

$left_sidebar = hemelios_get_post_meta_box_option( $prefix . 'top_left_sidebar' );
if ( ( $left_sidebar == '' ) || ( $left_sidebar == '-1' ) ) {
	$left_sidebar = $hemelios_options['top_bar_left_sidebar'];
}

$right_sidebar = hemelios_get_post_meta_box_option( $prefix . 'top_right_sidebar' );
if ( ( $right_sidebar == '' ) || ( $right_sidebar == '-1' ) ) {
	$right_sidebar = $hemelios_options['top_bar_right_sidebar'];
}

$top_bar_class = array( 'top-bar' );
if ( $hemelios_options['mobile_header_top_bar'] == '0' ) {
	$top_bar_class [] = 'mobile-top-bar-hide';
}

$col_top_bar_left  = '';
$col_top_bar_right = '';

if ( is_active_sidebar( $left_sidebar ) && is_active_sidebar( $right_sidebar ) ) {
	switch ( $top_bar_layout ) {
		case 'top-bar-1':
			$col_top_bar_left  = 'col-md-6';
			$col_top_bar_right = 'col-md-6';
			break;
		case 'top-bar-2':
			$col_top_bar_left  = 'col-md-8';
			$col_top_bar_right = 'col-md-4';
			break;
		case 'top-bar-3':
			$col_top_bar_left  = 'col-md-4';
			$col_top_bar_right = 'col-md-8';
			break;
	}

} else {
	if ( is_active_sidebar( $left_sidebar ) || is_active_sidebar( $right_sidebar ) ) {
		$col_top_bar_left  = 'col-md-12';
		$col_top_bar_right = 'col-md-12';
	}
}

if ( $top_bar_layout == 'top-bar-4' ) {
	$col_top_bar_left .= ' top-bar-center';
	$col_top_bar_right .= ' top-bar-center';
}

if ( empty( $col_top_bar_left ) ) {
	return; // NOT SHOW TOP BAR
}
?>
<div class="<?php echo join( ' ', $top_bar_class ); ?>">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<?php if ( is_active_sidebar( $left_sidebar ) ): ?>
						<div class="sidebar top-bar-left <?php echo esc_attr( $col_top_bar_left ) ?>">
							<?php dynamic_sidebar( $left_sidebar ); ?>
						</div>
					<?php endif; ?>
					<?php if ( is_active_sidebar( $right_sidebar ) ): ?>
						<div class="sidebar top-bar-right <?php echo esc_attr( $col_top_bar_right ) ?>">
							<?php dynamic_sidebar( $right_sidebar ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-sm-3 login-box text-right">
				<?php

					if( !is_user_logged_in() ){
						echo '<a id="login-link" href="javascript:;">Đăng nhập </a>';
					}else{
						$current_user = wp_get_current_user();
						echo 'Chào '. $current_user->display_name .' / <a href="'.wp_logout_url(home_url('/')).'"> Đăng xuất </a>';
					}
				?>
			</div>
		</div>
	</div>
</div>
