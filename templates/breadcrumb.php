<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/3/2015
 * Time: 10:20 AM
 */
$hemelios_options = hemelios_option();
$prefix                      = 'hemelios_';
$hemelios_breadcrumbs_position = hemelios_get_post_meta_box_option( $prefix . 'breadcrumbs_position' );

if ( ( $hemelios_breadcrumbs_position === '' ) || ( $hemelios_breadcrumbs_position == '-1' ) ) {

	if ( is_singular() ) {
		switch ( get_post_type() ) {
			case 'page':
				$hemelios_breadcrumbs_position = $hemelios_options['page_breadcrumbs_position'];
				break;
			case 'product':
				$hemelios_breadcrumbs_position = $hemelios_options['single_product_breadcrumbs_position'];
				break;
			case 'portfolio':
				$hemelios_breadcrumbs_position = $hemelios_options['portfolio_breadcrumbs_position'];
				break;
			case 'post':
				$hemelios_breadcrumbs_position = $hemelios_options['single_blog_breadcrumbs_position'];
				break;
			case 'services':
				$hemelios_breadcrumbs_position = $hemelios_options['service_breadcrumbs_position'];
				break;
			case 'ourteam':
				$hemelios_breadcrumbs_position = $hemelios_options['ourteam_breadcrumbs_position'];
				break;
			default:
				$hemelios_breadcrumbs_position = 'not match post type';
		}
	}



	if ( is_archive() || is_search() || is_front_page() ) {
		$hemelios_breadcrumbs_position = $hemelios_options['archive_breadcrumbs_position'];

		if ( get_post_type() == 'product' ) {
			$hemelios_breadcrumbs_position = $hemelios_options['archive_product_breadcrumbs_position'];
		} else {
			if ( get_post_type() == 'portfolio' ) {
				$hemelios_breadcrumbs_position = $hemelios_options['portfolio_breadcrumbs_position'];
			} else {
				if ( get_post_type() == 'services' ) {
					$hemelios_breadcrumbs_position = $hemelios_options['service_breadcrumbs_position'];
				} else {
					if ( get_post_type() == 'ourteam' ) {
						$hemelios_breadcrumbs_position = $hemelios_options['ourteam_breadcrumbs_position'];
					}
				}
			}
		}
	}

}


global $class_breadcrumbs;

switch ( $hemelios_breadcrumbs_position ) {
	case '0':
		$class_breadcrumbs = 'breadcrumbs-left';
		break;
	case '1':
		$class_breadcrumbs = 'breadcrumbs-center';
		break;
	case '2':
		$class_breadcrumbs = 'breadcrumbs-right';
		break;
	default:
		$class_breadcrumbs = 'not match post type';
}

?>
<?php if ( !is_front_page() ) : ?>
	<?php hemelios_get_breadcrumb(); ?>
<?php else: ?>
	<ul class="breadcrumbs <?php echo esc_attr( $class_breadcrumbs ); ?>">
		<li class="first" typeof="v:Breadcrumb"><?php echo esc_html__( 'You are here: ', 'hemelios' ) ?></li>
		<li class="home">
			<a rel="v:url" href="<?php echo esc_html( home_url( '/' ) ) ?>" class="home"><?php echo esc_html__( 'Home', 'hemelios' ) ?></a>
		</li>
		<li><span><?php echo esc_html__( 'Blog', 'hemelios' ); ?></span></li>
	</ul>
<?php endif; ?>

