<?php
$header_search_box = hemelios_header_search_box();
$hemelios_options = hemelios_option();
$hemelios_archive_loop = hemelios_archive_loop();
$prefix               = 'hemelios_';
$hemelios_header_layout = hemelios_get_post_meta_box_option( $prefix . 'header_layout' );
if ( ( $hemelios_header_layout === '' ) || ( $hemelios_header_layout == '-1' ) ) {
	$hemelios_header_layout = $hemelios_options['header_layout'];
}

$mobile_header_search_box = $hemelios_options['mobile_header_search_box'];

// SHOW HEADER
$header_show_hide = hemelios_get_post_meta_box_option( $prefix . 'header_show_hide' );
if ( ( $header_show_hide === '' ) ) {
	$header_show_hide = '1';
}

if ( is_404() ) {
	$hemelios_header_layout = 'header-1';
}
?>
<?php
$enable_header_customize = hemelios_get_post_meta_box_option( $prefix . 'enable_header_customize' );
$header_search_box       = '0';
if ( $enable_header_customize == '1' ) {
	$page_header_customize = hemelios_get_post_meta_box_option( $prefix . 'header_customize' );
	if ( isset( $page_header_customize['enable'] ) && !empty( $page_header_customize['enable'] ) ) {
		$header_customize = explode( '||', $page_header_customize['enable'] );
	}
	if ( isset( $header_customize ) ) {
		if ( in_array( 'search', $header_customize ) ) {
			$header_search_box = '1';
		}
	}
} else {
	if ( isset( $hemelios_options['header_customize'] ) && isset( $hemelios_options['header_customize']['enabled'] ) && is_array( $hemelios_options['header_customize']['enabled'] ) ) {
		if ( in_array( 'Search Box', $hemelios_options['header_customize']['enabled'] ) ) {
			$header_search_box = '1';
		}
	}
}
?>

<?php if ( ( $header_show_hide == '1' ) ): ?>
	<?php hemelios_get_template( 'header/' . $hemelios_header_layout ); ?>
	<?php if ( ( $header_search_box == '1' ) || ( $mobile_header_search_box == '1' ) ): ?>
		<?php hemelios_get_template( 'header/search', 'popup' ); ?>
	<?php endif; ?>
	<?php hemelios_get_template( 'header/get-quote-popup' ); ?>

<?php endif; ?>