<?php
$hemelios_options = hemelios_option();
$logo_url =  get_template_directory_uri()  . '/assets/images/theme-options/logo.svg';

if ( isset( $hemelios_options['mobile_header_logo']['url'] ) && !empty( $hemelios_options['mobile_header_logo']['url'] ) ) {
	$logo_url = $hemelios_options['mobile_header_logo']['url'];
} else {
	if ( isset( $hemelios_options['logo']['url'] ) && !empty( $hemelios_options['logo']['url'] ) ) {
		$logo_url = $hemelios_options['logo']['url'];
	}
}

$mobile_header_layout = 'header-mobile-1';
if ( isset( $hemelios_options['mobile_header_layout'] ) && !empty( $hemelios_options['mobile_header_layout'] ) ) {
	$mobile_header_layout = $hemelios_options['mobile_header_layout'];
}

?>
<?php if ( $mobile_header_layout == 'header-mobile-2q' ): ?>
	<div class="header-mobile-before">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
			<img src="<?php echo esc_url( $logo_url ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" />
		</a>
	</div>
<?php endif; ?>