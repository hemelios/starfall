<?php
$hemelios_options = hemelios_option();
$icon_shopping_cart_class = array( 'shopping-cart-wrapper', 'header-customize-item' );
if ( $hemelios_options['mobile_header_shopping_cart'] == '0' ) {
	$icon_shopping_cart_class[] = 'mobile-hide-shopping-cart';
}

// GET VIEW CART SUBTOTAL OPTION
$view_cart_subtotal = hemelios_get_post_meta_box_option('hemelios_cart_subtotal' );


if ($view_cart_subtotal == '' || $view_cart_subtotal == '-1') {
	if ( isset( $hemelios_options['view_cart_subtotal'] ) ) {
		$view_cart_subtotal = $hemelios_options['view_cart_subtotal'];
	} else {
		$view_cart_subtotal = '0';
	}
}

if (is_404()) {
	$view_cart_subtotal = '0';
}

//GET VIEW CART SUBTOTAL CLASS
if ( $view_cart_subtotal == '0' ) {
	$icon_shopping_cart_class[] = 'disable-cart-subtotal';
}



?>
<div class="<?php echo join( ' ', $icon_shopping_cart_class ); ?>">
	<div class="widget_shopping_cart_content ">
		<?php get_template_part( 'woocommerce/cart/mini-cart' ); ?>
	</div>
</div>