<?php
$hemelios_options = hemelios_option();

// get header mobile layout
$mobile_header_layout = 'header-mobile-1';
if ( isset( $hemelios_options['mobile_header_layout'] ) && !empty( $hemelios_options['mobile_header_layout'] ) ) {
	$mobile_header_layout = $hemelios_options['mobile_header_layout'];
}
$header_mobile_class = array( 'header-mobile-inner', $mobile_header_layout );

// Get logo url for mobile
$logo_url =  get_template_directory_uri()  . '/assets/images/theme-options/logo.png';
if ( isset( $hemelios_options['mobile_header_logo']['url'] ) && !empty( $hemelios_options['mobile_header_logo']['url'] ) ) {
	$logo_url = $hemelios_options['mobile_header_logo']['url'];
} else {
	if ( isset( $hemelios_options['logo']['url'] ) && !empty( $hemelios_options['logo']['url'] ) ) {
		$logo_url = $hemelios_options['logo']['url'];
	}
}

// Get search & mini-cart for header mobile
$prefix = 'hemelios_';

$mobile_header_shopping_cart = hemelios_get_post_meta_box_option( $prefix . 'mobile_header_shopping_cart' );
if ( ( $mobile_header_shopping_cart === '' ) || ( $mobile_header_shopping_cart == '-1' ) ) {
	$mobile_header_shopping_cart = $hemelios_options['mobile_header_shopping_cart'];
}

$mobile_header_search_box = hemelios_get_post_meta_box_option( $prefix . 'mobile_header_search_box' );
if ( ( $mobile_header_search_box === '' ) || ( $mobile_header_search_box == '-1' ) ) {
	$mobile_header_search_box = $hemelios_options['mobile_header_search_box'];
}

$mobile_header_menu_drop = 'drop';
if ( isset( $hemelios_options['mobile_header_menu_drop'] ) && !empty( $hemelios_options['mobile_header_menu_drop'] ) ) {
	$mobile_header_menu_drop = $hemelios_options['mobile_header_menu_drop'];
}

?>
<div class="container header-mobile-wrapper">
	<div class="<?php echo join( ' ', $header_mobile_class ) ?>">
		<div class="toggle-icon-wrapper" data-ref="main-menu" data-drop-type="<?php echo esc_attr( $mobile_header_menu_drop ); ?>">
			<div class="toggle-icon"><span></span></div>
		</div>

		<div class="header-customize">
			<?php if ( $mobile_header_search_box == '1' ): ?>
				<?php hemelios_get_template( 'header/search-button' ); ?>
			<?php endif; ?>
			<?php if ( ( $mobile_header_shopping_cart == '1' ) && class_exists( 'WooCommerce' ) ): ?>
				<?php hemelios_get_template( 'header/mini-cart' ); ?>
			<?php endif; ?>
		</div>

		<?php //if ($mobile_header_layout != 'header-mobile-2'): ?>
		<div class="header-logo-mobile">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
				<img src="<?php echo esc_url( $logo_url ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" />
			</a>
		</div>
		<?php //endif;?>
	</div>
</div>