<?php
// don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'hemeliosFramework_Check_Level_Shortcode' ) ) {
	class hemeliosFramework_Check_Level_Shortcode {
		function __construct() {
			add_shortcode( 'check_level', array( $this, 'check_level_shortcode' ) );
		}

		function check_level_shortcode( $atts ) {
			$layout_style = $el_class = $hemelios_animation = $css_animation = $duration = $delay = $styles_animation = '';
			extract( shortcode_atts( array(
				'layout_style'  => 'style1',
				'el_class'      => '',
				'css_animation' => '',
				'duration'      => '',
				'delay'         => ''
			), $atts ) );
			$hemelios_animation .= ' ' . esc_attr( $el_class );
			$hemelios_animation .= hemeliosFramework_Shortcodes::hemelios_get_css_animation( $css_animation );
			ob_start(); ?>
			<div class="check-level-shortcode <?php echo esc_attr( $layout_style ) ?> <?php echo esc_attr( $el_class ) ?> <?php echo esc_attr( $hemelios_animation ) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation( $duration, $delay ); ?> >
					<form action="" id="form_test_resgister">
						<div class="check-level-wrapper">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-wrapper">
										<input type="text" name="name_child" id="name_child" placeholder="Họ và tên trẻ" required />
									</div>
									<div class="input-wrapper">
										<input type="text" name="name_date" id="name_date" placeholder="Ngày sinh của trẻ" />
									</div>
									<div class="input-wrapper">
										<input type="text" name="join_time" id="join_time" placeholder="Bé đã học tiếng Anh trong bao lâu?" required />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-wrapper">
										<input type="text" name="you_name" id="you_name" placeholder="Họ tên phụ huynh" required />
									</div>
									<div class="input-wrapper">
										<input type="text" name="you_phone" id="you_phone" placeholder="Số điện thoại phụ huynh" required />
									</div>
									<div class="input-wrapper">
										<input type="email" name="you_email" id="you_email" placeholder="Email phụ huynh" />
									</div>
								</div>
								<div class="col-md-4 col-sm-12 col-xs-12">
									<div class="input-wrapper">
										<textarea name="you_message" id="you_message" cols="30" rows="2" placeholder="Câu hỏi dành cho starfall"></textarea>
									</div>
									<div class="input-wrapper text-right">
										<input type="submit" name="sumit_register" id="sumit_register" value="Đăng ký" />
									</div>
								</div>
							</div>
						</div>
					</form>

			</div>
			<?php
			$content = ob_get_clean();

			return $content;
		}
	}

	new hemeliosFramework_Check_Level_Shortcode();
}