<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 5/4/2016
 * Time: 8:18 AM
 */
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'ViettitanFramework_Shortcode_Starfall_Com' ) ) {
	class ViettitanFramework_Shortcode_Starfall_Com {
		function __construct() {
			add_shortcode( 'hemelios_starfall_com', array( $this, 'starfall_com_shortcode' ) );
		}

		function starfall_com_shortcode( $atts ) {

			$atts = vc_map_get_attributes( 'hemelios_starfall_com', $atts );
			extract( $atts );

			$hemelios_options = hemelios_option();
			$min_suffix_css   = ( isset( $hemelios_options['enable_minifile_css'] ) && $hemelios_options['enable_minifile_css'] == 1 ) ? '.min' : '';
			wp_enqueue_style( 'hemelios_starfall_com_css', PLUGIN_HEMELIOS_FRAMEWORK_URI . 'includes/shortcodes/starfall.com/assets/starfall' . $min_suffix_css . '.css', array(), false );

			$user_starfall = $hemelios_options['starfall_account'];
			$user_pass = $hemelios_options['starfall_password'];
			$user_ID          = get_current_user_id();
			$expiration_date = get_the_author_meta( 'expiration_date', $user_ID );
			$now             = date( 'd-m-Y' );
			$limit_date = '';

			if( strtotime($now) <= strtotime($expiration_date) ){
				if ( $expiration_date ) {
					$limit_date =  viettitan_subdate( $expiration_date, $now );
				}
			}

			ob_start(); ?>
			<div class="starfall_com_wrapper">
				<div class="entry-header">
					<h3>Đăng nhập tài khoản học online</h3>
				</div>
				<?php
					if( $limit_date != '' && $limit_date >= 1 && is_user_logged_in() ) :
				?>
					<form action="https://more.starfall.com/memb/login2.php" id="autologin" class="form-horizontal" method="post" accept-charset="utf-8">
						<div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
							<input type="hidden" name="Email" value="<?php echo esc_html($user_starfall) ?>" />
							<input type="hidden" name="Password"  value="<?php echo esc_html($user_pass) ?>" />
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8 text-center">
								<button type="submit" name="LOGIN" value="login" class="os-button size-md style1">Đăng nhập vào Starfall.com</button>
							</div>
						</div>
					</form>
				<?php else : ?>
					<form action="https://more.starfall.com/memb/login2.php" id="autologin" class="form-horizontal" method="post" accept-charset="utf-8">
						<div style="display:none;"><input type="hidden" name="_method" value="POST"></div>
						<div class="form-group">
							<label class="col-sm-offset-1 col-sm-2 control-label">Email/Tài khoản</label>
							<div class="col-sm-8">
								<input type="text" name="Email" placeholder="Nhập Email/Tài khoản" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-offset-1 col-sm-2 control-label">Mật khẩu</label>
							<div class="col-sm-8">
								<input type="password" name="Password" placeholder="Nhập Mật khẩu">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-8 text-right">
								<button type="submit" name="LOGIN" value="login" class="os-button size-md style1">Đăng nhập</button>
							</div>
						</div>
					</form>
				<?php endif; ?>
			</div>
			<?php
			$content = ob_get_clean();

			return $content;
		}
	}

	new ViettitanFramework_Shortcode_Starfall_Com();
}