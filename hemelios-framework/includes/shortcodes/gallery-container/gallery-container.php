<?php
// don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}
if ( !class_exists( 'hemeliosFramework_Shortcode_Gallery_Container' ) ) {
	class hemeliosFramework_Shortcode_Gallery_Container {
		function __construct() {
			add_shortcode( 'os_gallery_ctn', array( $this, 'gallery_ctn_shortcode' ) );
			add_shortcode( 'os_gallery_sc', array( $this, 'gallery_sc_shortcode' ) );
		}

		function gallery_ctn_shortcode( $atts, $content ) {
			$layout_style = $column_number = $el_class = $hemelios_animation = $css_animation = $duration = $delay = '';
			extract( shortcode_atts( array(
				'layout_style'  => 'style1',
				'column_number' => '3',
				'el_class'      => '',
				'css_animation' => '',
				'duration'      => '',
				'delay'         => ''
			), $atts ) );
			$hemelios_animation .= ' ' . esc_attr( $el_class );
			$hemelios_animation .= hemeliosFramework_Shortcodes::hemelios_get_css_animation( $css_animation );

			ob_start(); ?>

			<div class="os-gallerys-container clearfix col-<?php echo esc_attr( $column_number ) ?> <?php echo esc_attr( $layout_style ) ?>  <?php echo esc_attr( $hemelios_animation ) ?>" <?php echo hemeliosFramework_Shortcodes::hemelios_get_style_animation( $duration, $delay ); ?>>
				<?php echo do_shortcode( $content ) ?>
			</div>
			<?php
			$output = ob_get_clean();

			return $output;
		}

		function gallery_sc_shortcode( $atts, $content = null ) {
			$images_gallery = $gallery_type = $video_link = $videos_thumbnail = $title = $class = '';
			extract( shortcode_atts( array(
				'title'            => '',
				'gallery_type'     => 'gallery',
				'videos_thumbnail' => '',
				'video_link'       => '',
				'images_gallery'   => '',
			), $atts ) );
			$session_id = uniqid();
			ob_start(); ?>
			<div class="os-gallery-item type-<?php echo $gallery_type ?>">
				<div class="hemelios-gallary-thumb">
					<?php
					if ( $gallery_type == 'gallery' ) :
						$image_array = explode( ",", $images_gallery );
						if ( $image_array ) :
							$key = 0;
							foreach ( $image_array as $image ) :
								$key ++;
								$thumbnail_url = '';
								$width         = 390;
								$height        = 290;
								$post = get_post($image);
								if ( !empty( $image ) ) {
									$images_attr = wp_get_attachment_image_src( $image, "full" );

									if ( isset( $images_attr ) ) {
										$resize = matthewruddy_image_resize( $images_attr[0], $width, $height );
										if ( $resize != null ) {
											$thumbnail_url = $resize['url'];
										}
									}
								}
								if ( $key == 1 ) {
								} else {
									$class = 'hidden';
								}
								?>
								<div class="gallery-inner-thumb">
									<?php
									if ( $key == 1 ) {
										?>
										<img alt="<?php echo $post->post_title; ?>" src="<?php echo esc_url( $thumbnail_url ); ?>">
										<?php
									}
									?>
									<div class="gallery-hover <?php echo esc_attr( $class ) ?>">
										<div class="entry-header">
											<?php if ( $title != '' ) {
												echo '<h3>' . esc_html( $title ) . '</h3>';
											} ?>
										</div>
										<a data-rel="prettyPhoto[<?php echo $session_id ?>]" href="<?php echo esc_url($images_attr[0]) ?>">
											<i class="fa fa-search"></i>
										</a>
									</div>
								</div>

								<?php
							endforeach;
						endif;
					else :

						if ( $videos_thumbnail ) :
							$thumbnail_url = '';
							$width = 390;
							$height = 290;
							$images_attr = wp_get_attachment_image_src( $videos_thumbnail, "full" );
							if ( isset( $images_attr ) ) {
								$resize = matthewruddy_image_resize( $images_attr[0], $width, $height );
								if ( $resize != null ) {
									$thumbnail_url = $resize['url'];
								}
							}

							?>
							<div class="gallery-inner-thumb">
								<?php
								?>
								<img alt="" src="<?php echo esc_url( $thumbnail_url ); ?>">
								<?php
								?>
								<div class="gallery-hover <?php echo esc_attr( $class ) ?>">
									<div class="entry-header">
										<?php if ( $title != '' ) {
											echo '<h3>' . esc_html( $title ) . '</h3>';
										} ?>
									</div>
									<a data-rel="prettyPhoto[<?php echo $session_id ?>]" href="<?php echo esc_url( $video_link ) ?>">
										<i class="fa fa-play"></i>
									</a>
								</div>
							</div>

							<?php
						endif;
					endif;
					?>
				</div>
			</div>
			<?php
			$output = ob_get_clean();

			return $output;
		}
	}

	new hemeliosFramework_Shortcode_Gallery_Container();
}
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_os_gallery_ctn extends WPBakeryShortCodesContainer {
	}
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
	class WPBakeryShortCode_os_gallery_sc extends WPBakeryShortCode {
	}
}
?>
