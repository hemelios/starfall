<?php
function xmenu_get_transition() {
	return array(
		'none' => esc_html__('None','hemelios'),
		'x-animate-slide-up' => esc_html__('Slide Up','hemelios'),
		'x-animate-slide-down' => esc_html__('Slide Down','hemelios'),
		'x-animate-slide-left' => esc_html__('Slide Left','hemelios'),
		'x-animate-slide-right' => esc_html__('Slide Right','hemelios'),
		'x-animate-sign-flip' => esc_html__('Sign Flip','hemelios'),
	);
}

function xmenu_get_grid () {
	return array(
		'basic' => array(
			'text' => esc_html__('Basic','hemelios'),
			'options' => array(
				'auto' => esc_html__('Automatic','hemelios'),
				'x-col x-col-12-12' => esc_html__('Full Width','hemelios'),
			)
		),
		'halves' => array(
			'text' => esc_html__('Halves','hemelios'),
			'options' => array(
				'x-col x-col-6-12' => esc_html__('1/2','hemelios'),
			)
		),
		'thirds' => array(
			'text' => esc_html__('Thirds','hemelios'),
			'options' => array(
				'x-col x-col-4-12' => esc_html__('1/3','hemelios'),
				'x-col x-col-8-12' => esc_html__('2/3','hemelios'),
			)
		),
		'quarters' => array(
			'text' => __('Quarters','hemelios'),
			'options' => array(
				'x-col x-col-3-12' => esc_html__('1/4','hemelios'),
				'x-col x-col-9-12' => esc_html__('3/4','hemelios'),
			)
		),
		'fifths' => array(
			'text' => esc_html__('Fifths','hemelios'),
			'options' => array(
				'x-col x-col-2-10' => esc_html__('1/5','hemelios'),
				'x-col x-col-4-10' => esc_html__('2/5','hemelios'),
				'x-col x-col-6-10' => esc_html__('3/5','hemelios'),
				'x-col x-col-8-10' => esc_html__('4/5','hemelios'),
			)
		),
		'sixths' => array(
			'text' => esc_html__('Sixths','hemelios'),
			'options' => array(
				'x-col x-col-2-12' => esc_html__('1/6','hemelios'),
				'x-col x-col-10-12' => esc_html__('5/6','hemelios'),
			)
		),
		'sevenths' => array(
			'text' => __('Sevenths','hemelios'),
			'options' => array(
				'x-col x-col-1-7' => esc_html__('1/7','hemelios'),
				'x-col x-col-2-7' => esc_html__('2/7','hemelios'),
				'x-col x-col-3-7' => esc_html__('3/7','hemelios'),
				'x-col x-col-4-7' => esc_html__('4/7','hemelios'),
				'x-col x-col-5-7' => esc_html__('5/7','hemelios'),
				'x-col x-col-6-7' => esc_html__('6/7','hemelios'),
			)
		),
		'eighths' => array(
			'text' => esc_html__('Eighths','hemelios'),
			'options' => array(
				'x-col x-col-1-8' => esc_html__('1/8','hemelios'),
				'x-col x-col-3-8' => esc_html__('3/8','hemelios'),
				'x-col x-col-5-8' => esc_html__('5/8','hemelios'),
				'x-col x-col-7-8' => esc_html__('7/8','hemelios'),
			)
		),
		'ninths' => array(
			'text' => esc_html__('Ninths','hemelios'),
			'options' => array(
				'x-col x-col-1-9' => esc_html__('1/9','hemelios'),
				'x-col x-col-2-9' => esc_html__('2/9','hemelios'),
				'x-col x-col-4-9' => esc_html__('4/9','hemelios'),
				'x-col x-col-5-9' => esc_html__('5/9','hemelios'),
				'x-col x-col-7-9' => esc_html__('7/9','hemelios'),
				'x-col x-col-8-9' => esc_html__('8/9','hemelios'),
			)
		),
		'tenths' => array(
			'text' => esc_html__('Tenths','hemelios'),
			'options' => array(
				'x-col x-col-1-10' => esc_html__('1/10','hemelios'),
				'x-col x-col-3-10' => esc_html__('3/10','hemelios'),
				'x-col x-col-7-10' => esc_html__('7/10','hemelios'),
				'x-col x-col-9-10' => esc_html__('9/10','hemelios'),
			)
		),
		'elevenths' => array(
			'text' => __('Elevenths','hemelios'),
			'options' => array(
				'x-col x-col-1-11' => esc_html__('1/11','hemelios'),
				'x-col x-col-2-11' => esc_html__('2/11','hemelios'),
				'x-col x-col-3-11' => esc_html__('3/11','hemelios'),
				'x-col x-col-4-11' => esc_html__('4/11','hemelios'),
				'x-col x-col-5-11' => esc_html__('5/11','hemelios'),
				'x-col x-col-6-11' => esc_html__('6/11','hemelios'),
				'x-col x-col-7-11' => esc_html__('7/11','hemelios'),
				'x-col x-col-8-11' => esc_html__('8/11','hemelios'),
				'x-col x-col-9-11' => esc_html__('9/11','hemelios'),
				'x-col x-col-10-11' => esc_html__('10/11','hemelios'),
			)
		),
		'twelfths' => array(
			'text' => esc_html__('Twelfths','hemelios'),
			'options' => array(
				'x-col x-col-1-12' => esc_html__('1/12','hemelios'),
				'x-col x-col-5-12' => esc_html__('5/12','hemelios'),
				'x-col x-col-7-12' => esc_html__('7/12','hemelios'),
				'x-col x-col-11-12' => esc_html__('11/12','hemelios'),
			)
		),
	);
}


global $xmenu_item_settings;
$xmenu_item_settings = array(
	'general' => array(
		'text' => esc_html__('General','hemelios'),
		'icon' => 'fa fa-cogs',
		'config' => array(
			'general-heading' => array(
				'text' => esc_html__('General','hemelios'),
				'type' => 'heading'
			),
			'general-url' => array(
				'text' => esc_html__('URL','hemelios'),
				'type' => 'text',
				'std'  => '',
			),
			'general-title' => array(
				'text' => esc_html__('Navigation Label','hemelios'),
				'type' => 'text',
				'std'  => '',
			),
			'general-attr-title' => array(
				'text' => esc_html__('Title Attribute','hemelios'),
				'type' => 'text',
				'std'  => '',
			),
			'general-target' => array(
				'text' => esc_html__('Open link in a new window/tab','hemelios'),
				'type' => 'checkbox',
				'std'  => '',
				'value' => '_blank',
			),
			'general-classes' => array(
				'text' => esc_html__('CSS Classes (optional)','hemelios'),
				'type' => 'array',
				'std'  => '',
			),
			'general-xfn' => array(
				'text' => esc_html__('Link Relationship (XFN)','hemelios'),
				'type' => 'text',
				'std'  => '',
			),
			'general-description' => array(
				'text' => esc_html__('Description','hemelios'),
				'type' => 'textarea',
				'std'  => '',
			),
			'general-other-heading' => array(
				'text' => esc_html__('Other','hemelios'),
				'type' => 'heading'
			),
			'other-disable-text' => array(
				'text' => esc_html__('Disable Text','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'other-disable-menu-item' => array(
				'text' => esc_html__('Disable Menu Item','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'other-disable-link' => array(
				'text' => esc_html__('Disable Link','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'other-display-header-column' => array(
				'text' => esc_html__('Display as a Sub Menu column header','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'other-feature-text' => array(
				'text' => esc_html__('Menu Feature Text','hemelios'),
				'type' => 'text',
				'std' => ''
			),
		)
	),
	'icon' => array(
		'text' => esc_html__('Icon','hemelios'),
		'icon' => 'fa fa-qrcode',
		'config' => array(
			'icon-heading' => array(
				'text' => esc_html__('Icon','hemelios'),
				'type' => 'heading'
			),
			'icon-value' => array(
				'text' => esc_html__('Set Icon','hemelios'),
				'type' => 'icon',
				'std'  => '',
			),
			'icon-position' => array(
				'text' => esc_html__('Icon Position','hemelios'),
				'type' => 'select',
				'std'  => 'left',
				'options' => array(
					'left' => esc_html__('Left of Menu Text','hemelios'),
					'right' => esc_html__('Right of Menu Text','hemelios'),
				)
			),
			'icon-padding' => array(
				'text' => esc_html__('Padding Icon and Text Menu','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Padding between Icon and Text Menu (px). Do not include units','hemelios')
			)
		)
	),
	'image' => array(
		'text' => esc_html__('Image','hemelios'),
		'icon' => 'fa fa-picture-o',
		'config' => array(
			'image-heading' => array(
				'text' => esc_html__('Image','hemelios'),
				'type' => 'heading'
			),
			'image-url' => array(
				'text' => esc_html__('Image Url','hemelios'),
				'type' => 'image',
				'std'  => '',
			),
			'image-size' => array(
				'text' => esc_html__('Image Size','hemelios'),
				'type' => 'select',
				'std'  => 'inherit',
				'options' => xmenu_get_image_size()
			),
			'image-dimensions' => array(
				'text' => esc_html__('Image Dimensions','hemelios'),
				'type' => 'select',
				'std'  => 'inherit',
				'options' => array(
					'inherit' => 'Inherit from Menu Settings',
					'custom' => 'Custom',
				)
			),
			'image-width' => array(
				'text' => esc_html__('Image Width','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Image width attribute (px). Do not include units. Only valid if "Image Dimension" is set to "Custom" above','hemelios')
			),
			'image-height' => array(
				'text' => esc_html__('Image Height','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Image width attribute (px). Do not include units. Only valid if "Image Dimension" is set to "Custom" above','hemelios')
			),
			'image-layout' => array(
				'text' => esc_html__('Image Layout','hemelios'),
				'type' => 'select',
				'std'  => 'image-only',
				'options' => array(
					'image-only' => esc_html__('Image Only','hemelios'),
					'left' => esc_html__('Image Left','hemelios'),
					'right' => esc_html__('Image Right','hemelios'),
					'above' => esc_html__('Image Above','hemelios'),
					'below' => esc_html__('Image Below','hemelios'),
				)
			),
			'image-feature' => array(
				'text' => esc_html__('Use Feature Image','hemelios'),
				'type' => 'checkbox',
				'std'  => '',
				'des' => 'Use Feature Image from Post/Page Menu Item',
			),
		)
	),

	'layout' => array(
		'text' => esc_html__('Layout','hemelios'),
		'icon' => 'fa fa-columns',
		'config' => array(
			'layout-heading' => array(
				'text' => esc_html__('Layout','hemelios'),
				'type' => 'heading'
			),
			'layout-width' => array(
				'text' => esc_html__('Menu Item Width','hemelios'),
				'type' => 'select-group',
				'std'  => 'auto',
				'options' => xmenu_get_grid()
			),
			'layout-text-align' => array(
				'text' => esc_html__('Item Content Alignment','hemelios'),
				'type' => 'select',
				'std'  => 'none',
				'options' => array(
					'none' => esc_html__('Default','hemelios'),
					'left' => esc_html__('Text Left','hemelios'),
					'center' => esc_html__('Text Center','hemelios'),
					'right' => esc_html__('Text Right','hemelios'),
				)
			),
			'layout-padding' => array(
				'text' => esc_html__('Padding','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Set padding for menu item. Include the units.','hemelios'),
			),
			'layout-margin' => array(
				'text' => esc_html__('Margin','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Set margin for menu item. Include the units.','hemelios'),
			),
			'layout-new-row' => array(
				'text' => esc_html__('New Row','hemelios'),
				'type' => 'checkbox',
				'std'  => ''
			),
		)
	),
	'submenu' => array(
		'text' => esc_html__('Sub Menu','hemelios'),
		'icon' => 'fa fa-list-alt',
		'config' => array(
			'submenu-heading' => array(
				'text' => esc_html__('Sub Menu','hemelios'),
				'type' => 'heading'
			),
			'submenu-type' => array(
				'text' => esc_html__('Sub Menu Type','hemelios'),
				'type' => 'select',
				'std'  => 'standard',
				'options' => array(
					'standard' => esc_html__('Standard','hemelios'),
					'multi-column' => esc_html__('Multi Column','hemelios'),
					/*'stack' => esc_html__('Stack','hemelios'),*/
					'tab' => esc_html__('Tab','hemelios'),
				)
			),
			'submenu-position' => array(
				'text' => esc_html__('Sub Menu Position','hemelios'),
				'type' => 'select',
				'std'  => '',
				'options' => array(
					'' => esc_html__('Automatic','hemelios'),
					'pos-left-menu-parent' => esc_html__('Left of Menu Parent','hemelios'),
					'pos-right-menu-parent' => esc_html__('Right of Menu Parent','hemelios'),
					'pos-center-menu-parent' => esc_html__('Center of Menu Parent','hemelios'),
					'pos-left-menu-bar' => esc_html__('Left of Menu Bar','hemelios'),
					'pos-right-menu-bar' => esc_html__('Right of Menu Bar','hemelios'),
					'pos-full' => esc_html__('Full Size','hemelios'),
				)
			),
			'submenu-width-custom' => array(
				'text' => esc_html__('Sub Menu Width Custom','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Set custom Sub Menu Width. Include the units (px/em/%).','hemelios'),
			),
			'submenu-col-width-default' => array(
				'text' => esc_html__('Sub Menu Column Width Default','hemelios'),
				'type' => 'select-group',
				'std'  => 'auto',
				'options' => xmenu_get_grid()
			),
			'submenu-col-spacing-default' => array(
				'text' => esc_html__('Sub Menu Column Spacing Default','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Set sub menu column spacing default. Do not include unit.','hemelios'),
			),
			'submenu-list-style' => array(
				'text' => esc_html__('Sub Menu List Style','hemelios'),
				'type' => 'select',
				'std'  => 'none',
				'options' => array(
					'none' => esc_html__('None','hemelios'),
					'disc' => esc_html__('Disc','hemelios'),
					'square' => esc_html__('Square','hemelios'),
					'circle' => esc_html__('Circle','hemelios'),
				)
			),
			'submenu-tab-position' => array(
				'text' => esc_html__('Tab Position','hemelios'),
				'type' => 'select',
				'std'  => 'left',
				'des' => esc_html__('Tab Position set to "Sub Menu Type" is "TAB".','hemelios'),
				'options' => array(
					'left' => esc_html__('Left','hemelios'),
					'right' => esc_html__('Right','hemelios'),
				)
			),
			'submenu-animation' => array(
				'text' => esc_html__('Sub Menu Animation','hemelios'),
				'type' => 'select',
				'std'  => 'x-animate-sign-flip',
				'options' => xmenu_get_transition()
			),
		)
	),
	'custom-content' => array(
		'text' => esc_html__('Custom Content','hemelios'),
		'icon' => 'fa fa-code',
		'config' => array(
			'custom-content-heading' => array(
				'text' => esc_html__('Custom Content','hemelios'),
				'type' => 'heading'
			),
			'custom-content-value' => array(
				'text' => esc_html__('Custom Content','hemelios'),
				'type' => 'textarea',
				'std'  => '',
				'des' => esc_html__('Can contain HTML and shortcodes','hemelios'),
				'height' => '300px'
			),
		)
	),
	'widget' => array(
		'text' => esc_html__('Widget Area','hemelios'),
		'icon' => 'fa-puzzle-piece',
		'config' => array(
			'widget-heading' => array(
				'text' => esc_html__('Widget Area','hemelios'),
				'type' => 'heading'
			),
			'widget-area' => array(
				'text' => esc_html__('Widget Area','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Enter a name for your Widget Area, and a widget area specifically for this menu item will be automatically be created in the <a href="widgets.php" target="_blank">Widgets Screen</a>','hemelios'),
			),
		)
	),
	'customize-style' => array(
		'text' => esc_html__('Customize Style','hemelios'),
		'icon' => 'fa-paint-brush',
		'config' => array(
			'custom-style-menu-heading' => array(
				'text' => esc_html__('Menu Item','hemelios'),
				'type' => 'heading'
			),
			'custom-style-menu-bg-color' => array(
				'text' => esc_html__('Background Color','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-menu-text-color' => array(
				'text' => esc_html__('Text Color','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-menu-bg-color-active' => array(
				'text' => esc_html__('Background Color [Active]','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-menu-text-color-active' => array(
				'text' => esc_html__('Text Color [Active]','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-menu-bg-image' => array(
				'text' => esc_html__('Background Image','hemelios'),
				'type' => 'image',
				'std'  => '',
			),
			'custom-style-menu-bg-image-repeat' => array(
				'text' => esc_html__('Background Image Repeat','hemelios'),
				'type' => 'select',
				'std' => 'no-repeat',
				'hide-label' => 'true',
				'options' => array(
					'no-repeat' => 'no-repeat',
					'repeat' => 'repeat',
					'repeat-x' => 'repeat-x',
					'repeat-y' => 'repeat-y'
				)
			),
			'custom-style-menu-bg-image-attachment' => array(
				'text' => esc_html__('Background Image Attachment','hemelios'),
				'type' => 'select',
				'std' => 'scroll',
				'hide-label' => 'true',
				'options' => array(
					'scroll' => 'scroll',
					'fixed' => 'fixed'
				)
			),
			'custom-style-menu-bg-image-position' => array(
				'text' => esc_html__('Background Image Position','hemelios'),
				'type' => 'select',
				'std' => 'center',
				'hide-label' => 'true',
				'options' => array(
					'center' => 'center',
					'center left' => 'center left',
					'center right' => 'center right',
					'top left' => 'top left',
					'top center' => 'top center',
					'top right' => 'top right',
					'bottom left' => 'bottom left',
					'bottom center' => 'bottom center',
					'bottom right' => 'bottom right'
				)
			),
			'custom-style-menu-bg-image-size' => array(
				'text' => esc_html__('Background Image Size','hemelios'),
				'type' => 'select',
				'std' => 'auto',
				'hide-label' => 'true',
				'options' => array(
					'auto' => 'Keep original',
					'100% auto' => 'Stretch to width',
					'auto 100%' => 'Stretch to height',
					'cover' => 'Cover',
					'contain' => 'Contain'
				)
			),
			'custom-style-sub-menu-heading' => array(
				'text' => esc_html__('Sub Menu','hemelios'),
				'type' => 'heading'
			),
			'custom-style-sub-menu-bg-color' => array(
				'text' => esc_html__('Background Color','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-sub-menu-text-color' => array(
				'text' => esc_html__('Text Color','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-sub-menu-bg-image' => array(
				'text' => esc_html__('Background Image','hemelios'),
				'type' => 'image',
				'std'  => '',
			),
			'custom-style-sub-menu-bg-image-repeat' => array(
				'text' => esc_html__('Background Image Repeat','hemelios'),
				'type' => 'select',
				'std' => 'no-repeat',
				'hide-label' => 'true',
				'options' => array(
					'no-repeat' => 'no-repeat',
					'repeat' => 'repeat',
					'repeat-x' => 'repeat-x',
					'repeat-y' => 'repeat-y'
				)
			),
			'custom-style-sub-menu-bg-image-attachment' => array(
				'text' => esc_html__('Background Image Attachment','hemelios'),
				'type' => 'select',
				'std' => 'scroll',
				'hide-label' => 'true',
				'options' => array(
					'scroll' => 'scroll',
					'fixed' => 'fixed'
				)
			),
			'custom-style-sub-menu-bg-image-position' => array(
				'text' => esc_html__('Background Image Position','hemelios'),
				'type' => 'select',
				'std' => 'center',
				'hide-label' => 'true',
				'options' => array(
					'center' => 'center',
					'center left' => 'center left',
					'center right' => 'center right',
					'top left' => 'top left',
					'top center' => 'top center',
					'top right' => 'top right',
					'bottom left' => 'bottom left',
					'bottom center' => 'bottom center',
					'bottom right' => 'bottom right'
				)
			),
			'custom-style-sub-menu-bg-image-size' => array(
				'text' => esc_html__('Background Image Size','hemelios'),
				'type' => 'select',
				'std' => 'auto',
				'hide-label' => 'true',
				'options' => array(
					'auto' => 'Keep original',
					'100% auto' => 'Stretch to width',
					'auto 100%' => 'Stretch to height',
					'cover' => 'Cover',
					'contain' => 'Contain'
				)
			),
			'custom-style-col-min-width' => array(
				'text' => esc_html__('Column Min Width','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Set min-width for Sub Menu Column (px). Not include the units.','hemelios'),
			),
			'custom-style-padding' => array(
				'text' => esc_html__('Padding','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des' => esc_html__('Set padding for Sub Menu. Include the units.','hemelios'),
			),

			'custom-style-feature-menu-text-heading' => array(
				'text' => esc_html__('Menu Feature Text','hemelios'),
				'type' => 'heading'
			),
			'custom-style-feature-menu-text-type' => array(
				'text' => esc_html__('Feature Menu Type','hemelios'),
				'type' => 'select',
				'std'  => '',
				'options' => array(
					'' => esc_html__('Standard','hemelios'),
					'x-feature-menu-not-float' => esc_html__('Not Float','hemelios')
				)
			),
			'custom-style-feature-menu-text-bg-color' => array(
				'text' => esc_html__('Background Color','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-feature-menu-text-color' => array(
				'text' => esc_html__('Text Color','hemelios'),
				'type' => 'color',
				'std'  => '',
			),
			'custom-style-feature-menu-text-top' => array(
				'text' => esc_html__('Position Top','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des'  => 'Position Top (px) Feature Menu Text. Do not include units.',
			),
			'custom-style-feature-menu-text-left' => array(
				'text' => esc_html__('Position Left','hemelios'),
				'type' => 'text',
				'std'  => '',
				'des'  => 'Position Left (px) Feature Menu Text. Do not include units.',
			),
		)
	),
	'responsive' => array(
		'text' => esc_html__('Responsive','hemelios'),
		'icon' => 'fa-desktop',
		'config' => array(
			'responsive-heading' => array(
				'text' => esc_html__('Responsive','hemelios'),
				'type' => 'heading'
			),
			'responsive-hide-mobile-css' => array(
				'text' => esc_html__('Hide item on mobile via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-hide-desktop-css' => array(
				'text' => esc_html__('Hide item on desktop via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-hide-mobile-css-submenu' => array(
				'text' => esc_html__('Hide sub menu on mobile via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-remove-mobile' => array(
				'text' => esc_html__('Remove this item when mobile device is detected via wp_is_mobile()','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-remove-desktop' => array(
				'text' => esc_html__('Remove this item when desktop device is NOT detected via wp_is_mobile()','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-remove-mobile-submenu' => array(
				'text' => esc_html__('Remove sub menu when desktop device is NOT detected via wp_is_mobile()','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
		),
	),
	'responsive' => array(
		'text' => esc_html__('Responsive','hemelios'),
		'icon' => 'fa-desktop',
		'config' => array(
			'responsive-heading' => array(
				'text' => esc_html__('Responsive','hemelios'),
				'type' => 'heading'
			),
			'responsive-hide-mobile-css' => array(
				'text' => esc_html__('Hide item on mobile via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-hide-desktop-css' => array(
				'text' => esc_html__('Hide item on desktop via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-hide-mobile-css-submenu' => array(
				'text' => esc_html__('Hide sub menu on mobile via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-hide-desktop-css-submenu' => array(
				'text' => esc_html__('Hide sub menu on desktop via CSS','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			/*'responsive-remove-mobile' => array(
				'text' => __('Remove this item when mobile device is detected via wp_is_mobile()','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-remove-desktop' => array(
				'text' => __('Remove this item when desktop device is NOT detected via wp_is_mobile()','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),
			'responsive-remove-mobile-submenu' => array(
				'text' => __('Remove sub menu when desktop device is NOT detected via wp_is_mobile()','hemelios'),
				'type' => 'checkbox',
				'std' => ''
			),*/
		),
	)
);

global $xmenu_item_defaults;
$xmenu_item_defaults = xmenu_get_item_defaults($xmenu_item_settings);

function xmenu_get_item_defaults($items_setting, $defaults = array()) {
	if (!$defaults) {
		$defaults = array(
			'nosave-type_label' => '',
			'nosave-type' => '',
			'nosave-change' => 0
		);
	}

	foreach ($items_setting as $seting_key => $setting) {
		foreach ($setting['config'] as $key => $value) {
			if (isset($value['config']) && $value['config']) {

			}
			else {
				if ($value['type'] != 'heading') {
					$defaults[$key] = $value['std'];
				}
			}

		}
	}
	return $defaults;
}
function xmenu_get_image_size($is_setting = 0) {
	global $_wp_additional_image_sizes;

	$sizes = array();
	$get_intermediate_image_sizes = get_intermediate_image_sizes();

	// Create the full array with sizes and crop info
	foreach( $get_intermediate_image_sizes as $_size ) {

		if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

			$sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
			$sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
			$sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

			$sizes[ $_size ] = array(
				'width' => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
			);

		}

	}
	$image_size = array();
	if (!$is_setting) {
		$image_size ['inherit'] = esc_html__('Inherit from Menu Setting','hemelios');
	}
	$image_size ['full'] = esc_html__('Full Size','hemelios');
	foreach ($sizes as $key => $value) {
		$image_size[$key] = ucfirst($key) . ' (' . $value['width'] . ' x ' . $value['height'] .')' . ($value['crop'] ? '[cropped]' : '') ;
	}
	return $image_size;
}