<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/19/2015
 * Time: 3:59 PM
 */
if ( !function_exists( 'hemelios_custom_style_wp_head' ) ) {
	function hemelios_custom_style_wp_head() {
		echo '<style id="hemelios_custom_style"></style>';

	}

	add_action( 'wp_head', 'hemelios_custom_style_wp_head', 100 );
}

/*---------------------------------------------------
/* SEARCH AJAX
/*---------------------------------------------------*/
if ( !function_exists( 'hemelios_result_search_callback' ) ) {
	function hemelios_result_search_callback() {
		ob_start();

		$hemelios_options = hemelios_option();
		$posts_per_page = 8;

		if ( isset( $hemelios_options['search_box_result_amount'] ) && !empty( $hemelios_options['search_box_result_amount'] ) ) {
			$posts_per_page = $hemelios_options['search_box_result_amount'];
		}

		$post_type = array();
		if ( isset( $hemelios_options['search_box_post_type'] ) && is_array( $hemelios_options['search_box_post_type'] ) ) {
			foreach ( $hemelios_options['search_box_post_type'] as $key => $value ) {
				if ( $value == 1 ) {
					$post_type[] = $key;
				}
			}
		}


		$keyword = $_REQUEST['keyword'];

		if ( $keyword ) {
			$search_query = array(
				's'              => $keyword,
				'order'          => 'DESC',
				'orderby'        => 'date',
				'post_status'    => 'publish',
				'post_type'      => $post_type,
				'posts_per_page' => $posts_per_page + 1,
			);
			$search       = new WP_Query( $search_query );

			$newdata = array();
			if ( $search && count( $search->post ) > 0 ) {
				$count = 0;
				foreach ( $search->posts as $post ) {
					if ( $count == $posts_per_page ) {
						$newdata[] = array(
							'id'    => - 2,
							'title' => '<a href="' . site_url() . '?s=' . $keyword . '"><i class="fa fa-mail-forward"></i> ' . esc_html__( 'View More', 'hemelios' ) . '</a>',
							'guid'  => '',
							'date'  => null,
						);

						break;
					}
					$newdata[] = array(
						'id'    => $post->ID,
						'title' => $post->post_title,
						'guid'  => get_permalink( $post->ID ),
						'date'  => mysql2date( 'M d Y', $post->post_date ),
					);
					$count ++;

				}
			} else {
				$newdata[] = array(
					'id'    => - 1,
					'title' => esc_html__( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'hemelios' ),
					'guid'  => '',
					'date'  => null,
				);
			}

			ob_end_clean();
			echo json_encode( $newdata );
		}
		die(); // this is required to return a proper result
	}

	add_action( 'wp_ajax_nopriv_result_search', 'hemelios_result_search_callback' );
	add_action( 'wp_ajax_result_search', 'hemelios_result_search_callback' );

}

/*---------------------------------------------------
/* CUSTOM PAGE - LESS JS
/*---------------------------------------------------*/
if ( !function_exists( 'hemelios_custom_page_less_js' ) ) {
	function hemelios_custom_page_less_js() {
		echo hemelios_custom_css_variable();
		echo '@import "' . get_template_directory_uri() . '/assets/css/less/style.less";', PHP_EOL;
		$hemelios_options = hemelios_option();
		$home_preloader = $hemelios_options['home_preloader'];
		if ( $home_preloader != 'none' && !empty( $home_preloader ) ) {
			echo '@import "' . get_template_directory_uri() . '/assets/css/less/loading/' . $home_preloader . '.less";', PHP_EOL;
		}

		if ( isset( $hemelios_options['panel_selector'] ) && ( $hemelios_options['panel_selector'] == 1 ) ) {
			echo '@import "' . get_template_directory_uri() . '/assets/css/less/panel-style-selector.less";', PHP_EOL;
		}

		$enable_rtl_mode = '0';
		if ( isset( $hemelios_options['enable_rtl_mode'] ) ) {
			$enable_rtl_mode = $hemelios_options['enable_rtl_mode'];
		}

		if ( is_rtl() || $enable_rtl_mode == '1' || isset( $_GET['RTL'] ) ) {
			echo '@import "' . get_template_directory_uri() . '/assets/css/less/rtl.less";', PHP_EOL;
		}
	}

	add_action( 'custom-page/less-js', 'hemelios_custom_page_less_js' );
}


/*---------------------------------------------------
/* Add less script for developer
/*---------------------------------------------------*/
if ( !function_exists( 'hemelios_add_less_for_dev' ) ) {
	function hemelios_add_less_for_dev() {
		if ( defined( 'HEMELIOS_SCRIPT_DEBUG' ) && HEMELIOS_SCRIPT_DEBUG ) {
			echo '<link rel="stylesheet/less" type="text/css" href="' . get_template_directory_uri() . '/hemelios-less-css?custom-page=less-js' . ( isset( $_GET['RTL'] ) ? '&RTL=1' : '' ) . '"/>';
			echo '<script src="' . get_template_directory_uri() . '/assets/js/less-1.7.3.min.js"></script>';

			$css = hemelios_custom_css();
			echo '<style>' . $css . '</style>';
		}
	}

	add_action( 'wp_head', 'hemelios_add_less_for_dev', 100 );
}

/*---------------------------------------------------
/* Panel Selector
/*---------------------------------------------------*/
if ( !function_exists( 'hemelios_panel_selector_callback' ) ) {
	function hemelios_panel_selector_callback() {
		hemelios_get_template( 'panel-selector' );
		die();
	}

	add_action( 'wp_ajax_nopriv_panel_selector', 'hemelios_panel_selector_callback' );
	add_action( 'wp_ajax_panel_selector', 'hemelios_panel_selector_callback' );
}

if ( !function_exists( 'hemelios_panel_selector_change_color_callback' ) ) {
	function hemelios_panel_selector_change_color_callback() {
		if ( !class_exists( 'Less_Parser' ) ) {
			get_template_part( 'hemelios-framework/less/Autoloader' );
			Less_Autoloader::register();
		}
		$content_file  = hemelios_custom_css_variable();
		$primary_color = $_REQUEST['primary_color'];
		$content_file .= '@primary_color:' . $primary_color . ';';
		$content_file .= '@menu_text_hover_color:' . $primary_color . ';';
		$content_file .= '@link_color:' . $primary_color . ';';
		$content_file .= '@link_color_hover:' . $primary_color . ';';
		$content_file .= '@link_color_active:' . $primary_color . ';';
//		$file_full_variable =  get_template_directory()  . '/assets/css/less/mixin.less';
//		$file_color         =  get_template_directory()  . '/assets/css/less/color.less';
		$style_file = get_template_directory() . '/assets/css/less/style.less';

		$parser = new Less_Parser( array( 'compress' => true ) );
		$parser->parse( $content_file );
		$parser->parseFile( $style_file );
		$css = $parser->getCss();
		echo esc_attr($css);
		die();

	}

	add_action( 'wp_ajax_nopriv_custom_css_selector', 'hemelios_panel_selector_change_color_callback' );
	add_action( 'wp_ajax_custom_css_selector', 'hemelios_panel_selector_change_color_callback' );
}

/*---------------------------------------------------
/* Product Quick View
/*---------------------------------------------------*/
if ( !function_exists( 'hemelios_product_quick_view_callback' ) ) {
	function hemelios_product_quick_view_callback() {
		$product_id = $_REQUEST['id'];
		global $post, $product, $woocommerce;
		$post = get_post( $product_id );
		setup_postdata( $post );
		$product = wc_get_product( $product_id );
		wc_get_template_part( 'content-product-quick-view' );
		wp_reset_postdata();
		die();
	}

	add_action( 'wp_ajax_nopriv_product_quick_view', 'hemelios_product_quick_view_callback' );
	add_action( 'wp_ajax_product_quick_view', 'hemelios_product_quick_view_callback' );
}






