<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 10:39 AM
 */
/*================================================
GET TEMPLATE
================================================== */
if ( !function_exists( 'hemelios_get_template' ) ) {
	function hemelios_get_template( $template, $name = null ) {
		get_template_part( 'templates/' . $template, $name );
	}
}

/*================================================
GET POST META
================================================== */
if ( !function_exists( 'hemelios_get_post_meta' ) ) {
	function hemelios_get_post_meta( $id, $key = "", $single = false ) {

		$GLOBALS['hemelios_post_meta'] = isset( $GLOBALS['hemelios_post_meta'] ) ? $GLOBALS['hemelios_post_meta'] : array();
		if ( !isset( $id ) ) {
			return;
		}
		if ( !is_array( $id ) ) {
			if ( !isset( $GLOBALS['hemelios_post_meta'][$id] ) ) {
				//$GLOBALS['hemelios_post_meta'][ $id ] = array();
				$GLOBALS['hemelios_post_meta'][$id] = get_post_meta( $id );
			}
			if ( !empty( $key ) && isset( $GLOBALS['hemelios_post_meta'][$id][$key] ) && !empty( $GLOBALS['hemelios_post_meta'][$id][$key] ) ) {
				if ( $single ) {
					return maybe_unserialize( $GLOBALS['hemelios_post_meta'][$id][$key][0] );
				} else {
					return array_map( 'maybe_unserialize', $GLOBALS['hemelios_post_meta'][$id][$key] );
				}
			}

			if ( $single ) {
				return '';
			} else {
				return array();
			}

		}

		return get_post_meta( $id, $key, $single );
	}
}

/*
 *  GET POST META BOX OPTION
 */
if ( !function_exists( 'hemelios_get_post_meta_box_option' ) ) {
	function hemelios_get_post_meta_box_option( $key, $args = array(), $post_id = null ) {
		if ( is_singular() ) {
			return rwmb_meta( $key, $args, $post_id );
		}

		return '';
	}
}


/* GET USER MENU LIST
    ================================================== */
if ( !function_exists( 'hemelios_get_menu_list' ) ) {
	function hemelios_get_menu_list() {

		if ( !is_admin() ) {
			return array();
		}

		$user_menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );

		$menu_list = array();

		foreach ( $user_menus as $menu ) {
			$menu_list[$menu->term_id] = $menu->name;
		}

		return $menu_list;
	}
}

/* CHECK IS BLOG PAGE
    ================================================== */
if ( !function_exists( 'hemelios_is_blog_page' ) ) {
	function hemelios_is_blog_page() {
		global $post;

		//Post type must be 'post'.
		$post_type = get_post_type( $post );

		return (
			( is_home() || is_archive() || is_single() )
			&& ( $post_type == 'post' )
		) ? true : false;
	}
}

/* ATTRIBUTE VALUE
    ================================================== */
if ( !function_exists( 'hemelios_the_attr_value' ) ) {
	function hemelios_the_attr_value( $attr ) {
		foreach ( $attr as $key ) {
			echo esc_attr( $key ) . ' ';
		}
	}
}

/*================================================
MAINTENANCE MODE
================================================== */
if ( !function_exists( 'hemelios_maintenance_mode' ) ) {
	function hemelios_maintenance_mode() {

		if ( current_user_can( 'edit_themes' ) || is_user_logged_in() ) {
			return;
		}

		$hemelios_options     = hemelios_option();
		$enable_maintenance = isset( $hemelios_options['enable_maintenance'] ) ? $hemelios_options['enable_maintenance'] : 0;

		switch ( $enable_maintenance ) {
			case '1' :
				wp_die( '<p style="text-align:center">' . esc_html__( 'We are currently in maintenance mode, please check back shortly.', 'hemelios' ) . '</p>', get_bloginfo( 'name' ) );
				break;
			case '2':
				$maintenance_mode_page = $hemelios_options['maintenance_mode_page'];
				if ( empty( $maintenance_mode_page ) ) {
					wp_die( '<p style="text-align:center">' . esc_html__( 'We are currently in maintenance mode, please check back shortly.', 'hemelios' ) . '</p>', get_bloginfo( 'name' ) );
				} else {
					$maintenance_mode_page_url = get_permalink( $maintenance_mode_page );
					$current_page_url          = hemelios_current_page_url();
					if ( $maintenance_mode_page_url != $current_page_url ) {
						wp_redirect( $maintenance_mode_page_url );
					}
				}
				break;
		}
	}

	add_action( 'get_header', 'hemelios_maintenance_mode' );
}

/*================================================
GET CURRENT PAGE URL
================================================== */
if ( !function_exists( 'hemelios_current_page_url' ) ) {
	function hemelios_current_page_url() {
		$pageURL = 'http';
		if ( is_ssl()  ) {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		if ( $_SERVER["SERVER_NAME"] && $_SERVER["REQUEST_URI"] ) {
			$pageURL .= $_SERVER["SERVER_NAME"]  . $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
}

/*================================================
Hemelios Option
================================================== */
if ( !function_exists( 'hemelios_option' ) ) {
	function hemelios_option() {
		global $hemelios_options;
		return $hemelios_options;
	}
}

/*================================================
Hemelios archive loop
================================================== */
if ( !function_exists( 'hemelios_archive_loop' ) ) {
	function hemelios_archive_loop() {
		global $hemelios_archive_loop;
		return $hemelios_archive_loop;
	}
}

/*================================================
404 Custom Style Option
================================================== */
if ( !function_exists( 'hemelios_404_custom_style' ) ) {
	function hemelios_404_custom_style() {
		global $custom_404_style;
		return $custom_404_style;
	}
}

/*================================================
Hemelios hemelios_header_search_box
================================================== */
if ( !function_exists( 'hemelios_header_search_box' ) ) {
	function hemelios_header_search_box() {
		global $header_search_box;
		return $header_search_box;
	}
}

/*================================================
Hemelios hemelios_header_search_box
================================================== */
if ( !function_exists( 'hemelios_class_breadcrumbs' ) ) {
	function hemelios_class_breadcrumbs() {
		global $class_breadcrumbs;
		return $class_breadcrumbs;
	}
}
