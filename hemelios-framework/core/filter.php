<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/6/2015
 * Time: 9:36 AM
 */

/*================================================
FILTER SEARCH FORM
================================================== */
if ( !function_exists( 'hemelios_search_form' ) ) {
	function hemelios_search_form( $form ) {
		$form = '<form class="search-form" method="get" id="searchform" action="' . home_url( '/' ) . '">
                <input type="text" value="' . get_search_query() . '" name="s" id="s"  placeholder="' . esc_html__( "Search...", 'hemelios' ) . '">
                <button type="submit"><i class="fa fa-search"></i></button>
     		</form>';

		return $form;
	}

	add_filter( 'get_search_form', 'hemelios_search_form' );
}

/*================================================
FILTER TAG FORMAT
================================================== */
if ( !function_exists( 'hemelios_tag_cloud' ) ) {
	function hemelios_tag_cloud( $tag_string ) {
		return preg_replace( "/style='font-size:.+pt;'/", '', $tag_string );
	}

	add_filter( 'wp_generate_tag_cloud', 'hemelios_tag_cloud', 10, 3 );
}

/* CUSTOM PAGE TEMPLATE
    ================================================== */
if ( !function_exists( 'hemelios_page_template_custom' ) ) {
	function hemelios_page_template_custom( $template ) {
		if ( isset( $_REQUEST['custom-page'] ) && !empty( $_REQUEST['custom-page'] ) ) {
			do_action( 'custom-page/' . $_REQUEST['custom-page'] );

			return;
		}

		return $template;

	}

	add_filter( "page_template", "hemelios_page_template_custom" );
}