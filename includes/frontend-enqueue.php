<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/1/2015
 * Time: 6:16 PM
 */
/*================================================
LOAD STYLESHEETS
================================================== */
if ( !function_exists( 'hemelios_enqueue_styles' ) ) {
	function hemelios_enqueue_styles() {
		$hemelios_options = hemelios_option();
		$min_suffix     = ( isset( $hemelios_options['enable_minifile_css'] ) && $hemelios_options['enable_minifile_css'] == 1 ) ? '.min' : '';

		/*font-awesome*/
		$url_font_awesome =  get_template_directory_uri()  . '/assets/plugins/fonts-awesome/css/font-awesome.min.css';
		if ( isset( $hemelios_options['cdn_font_awesome'] ) && !empty( $hemelios_options['cdn_font_awesome'] ) ) {
			$url_font_awesome = $hemelios_options['cdn_font_awesome'];
		}
		wp_enqueue_style( 'hemelios_framework_font_awesome', $url_font_awesome, array() );
		wp_enqueue_style( 'hemelios_framework_font_awesome_animation',  get_template_directory_uri()  . '/assets/plugins/fonts-awesome/css/font-awesome-animation.min.css', array() );

		/*bootstrap*/
		$url_bootstrap =  get_template_directory_uri()  . '/assets/plugins/bootstrap/css/bootstrap.min.css';
		if ( isset( $hemelios_options['cdn_bootstrap_css'] ) && !empty( $hemelios_options['cdn_bootstrap_css'] ) ) {
			$url_bootstrap = $hemelios_options['cdn_bootstrap_css'];
		}
		wp_enqueue_style( 'hemelios_framework_bootstrap', $url_bootstrap, array() );

		wp_deregister_style( 'hemelios-jplayer-style' );
		wp_register_style( 'hemelios-jplayer-style',  get_template_directory_uri()  . '/assets/plugins/jquery.jPlayer/skin/hemelios/skin' . $min_suffix . '.css' );

		/*flat-icon*/
//		wp_enqueue_style('hemelios_framework_flat_icon',  get_template_directory_uri()  . '/assets/plugins/flaticon/css/flaticon.css', array());

		wp_enqueue_style( 'hemelios_framework_hemelios_icon',  get_template_directory_uri()  . '/assets/plugins/hemelios-icon/css/styles.css', array() );

		/*owl-carousel*/
		wp_enqueue_style( 'hemelios_framework_owl_carousel',  get_template_directory_uri()  . '/assets/plugins/owl-carousel/owl.carousel.min.css', array() );
		wp_enqueue_style( 'hemelios_framework_owl_carousel_theme',  get_template_directory_uri()  . '/assets/plugins/owl-carousel/owl.theme.min.css', array() );
		wp_enqueue_style( 'hemelios_framework_owl_carousel_transitions',  get_template_directory_uri()  . '/assets/plugins/owl-carousel/owl.transitions.css', array() );

		/*prettyPhoto*/
		wp_enqueue_style( 'hemelios_framework_prettyPhoto',  get_template_directory_uri()  . '/assets/plugins/prettyPhoto/css/prettyPhoto.css', array() );

		/*peffect_scrollbar*/
		wp_enqueue_style( 'hemelios_framework_peffect_scrollbar',  get_template_directory_uri()  . '/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css', array() );

		if ( !( defined( 'HEMELIOS_SCRIPT_DEBUG' ) && HEMELIOS_SCRIPT_DEBUG ) ) {
			wp_enqueue_style( 'hemelios_framework_style',  get_template_directory_uri()  . '/style' . $min_suffix . '.css' );
		}
		wp_enqueue_style( 'hemelios_framework_vc_customize_css',  get_template_directory_uri()  . '/assets/css/vc-customize' . $min_suffix . '.css' );

		$enable_rtl_mode = '0';
		if ( isset( $hemelios_options['enable_rtl_mode'] ) ) {
			$enable_rtl_mode = $hemelios_options['enable_rtl_mode'];
		}

		if ( is_rtl() || $enable_rtl_mode == '1' || isset( $_GET['RTL'] ) ) {
			wp_enqueue_style( 'hemelios_framework_rtl',  get_template_directory_uri()  . '/assets/css/rtl' . $min_suffix . '.css' );
		}

	}

	add_action( 'wp_enqueue_scripts', 'hemelios_enqueue_styles', 11 );
}

/*================================================
LOAD SCRIPTS
================================================== */
if ( !function_exists( 'hemelios_enqueue_script' ) ) {
	function hemelios_enqueue_script() {
		$hemelios_options = hemelios_option();
		$min_suffix     = ( isset( $hemelios_options['enable_minifile_js'] ) && $hemelios_options['enable_minifile_js'] == 1 ) ? '.min' : '';

		/*bootstrap*/
		$url_bootstrap =  get_template_directory_uri()  . '/assets/plugins/bootstrap/js/bootstrap.min.js';
		if ( isset( $hemelios_options['cdn_bootstrap_js'] ) && !empty( $hemelios_options['cdn_bootstrap_js'] ) ) {
			$url_bootstrap = $hemelios_options['cdn_bootstrap_js'];
		}
		wp_enqueue_script( 'hemelios_framework_bootstrap', $url_bootstrap, array( 'jquery' ), false, true );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style( 'jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );
		if ( is_single() ) {
			wp_enqueue_script( 'comment-reply' );
		}

		/*plugins*/
		wp_enqueue_script( 'hemelios_framework_plugins',  get_template_directory_uri()  . '/assets/js/plugin' . $min_suffix . '.js', array(), false, true );

		/*smooth-scroll*/
		if ( isset( $hemelios_options['smooth_scroll'] ) && ( $hemelios_options['smooth_scroll'] == 1 ) ) {
			wp_enqueue_script( 'hemelios_framework_smooth_scroll',  get_template_directory_uri()  . '/assets/plugins/smoothscroll/SmoothScroll' . $min_suffix . '.js', array(), false, true );
		}

		/*panel-selector*/
		if ( isset( $hemelios_options['panel_selector'] ) && ( $hemelios_options['panel_selector'] == 1 ) ) {
			wp_enqueue_script( 'hemelios_framework_panel_selector',  get_template_directory_uri()  . '/assets/js/panel-style-selector' . $min_suffix . '.js', array(), false, true );
		}

		wp_enqueue_script( 'hemelios_framework_app',  get_template_directory_uri()  . '/assets/js/app' . $min_suffix . '.js', array(), false, true );
		wp_enqueue_script( 'hemelios_framework_myscript',  get_template_directory_uri()  . '/assets/js/myscript' . $min_suffix . '.js', array(), false, true );

		// Localize the script with new data
		$translation_array = array(
			'product_compare'  => esc_html__( 'Compare', 'hemelios' ),
			'product_wishList' => esc_html__( 'WishList', 'hemelios' )
		);
		wp_localize_script( 'hemelios_framework_app', 'hemelios_framework_constant', $translation_array );

		wp_localize_script( 'hemelios_framework_app', 'hemelios_framework_ajax_url', get_site_url() . '/wp-admin/admin-ajax.php?activate-multi=true' );
		wp_localize_script( 'hemelios_framework_app', 'hemelios_framework_theme_url', trailingslashit( get_template_directory_uri() ) );
		wp_localize_script( 'hemelios_framework_app', 'hemelios_framework_site_url', site_url() );

	}

	add_action( 'wp_enqueue_scripts', 'hemelios_enqueue_script' );
}

/* CUSTOM CSS OUTPUT
	================================================== */
if ( !function_exists( 'hemelios_enqueue_custom_css' ) ) {
	function hemelios_enqueue_custom_css() {
		$hemelios_options = hemelios_option();
		$custom_css     = $hemelios_options['custom_css'];
		if ( $custom_css ) {
			echo sprintf( '<style type="text/css">%s %s</style>', "\n", $custom_css );
		}
	}

	add_action( 'wp_head', 'hemelios_enqueue_custom_css' );
}

/* CUSTOM JS OUTPUT
	================================================== */
if ( !function_exists( 'hemelios_enqueue_custom_script' ) ) {
	function hemelios_enqueue_custom_script() {
		$hemelios_options = hemelios_option();
		$custom_js      = $hemelios_options['custom_js'];
		if ( $custom_js ) {
			echo sprintf( '<script type="text/javascript">%s</script>', $custom_js );
		}
	}

	add_action( 'wp_footer', 'hemelios_enqueue_custom_script' );
}
