<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 4/25/2016
 * Time: 4:56 PM
 */
global $meta_boxes;

/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function starfall_register_meta_boxes() {
	global $meta_boxes;
	$prefix = 'hemelios_';

	$hemelios_options = hemelios_option();
	$level   = $hemelios_options['level_list'];
	$level_list = array();
	if( $level ){
		foreach( $level as $value ){
			$level_list[sanitize_title($value)] = $value;
		}
	}
	$page_list = array();
	$pages = get_pages();
	foreach ( $pages as $page ) {
		$page_list[$page->ID] = $page->post_title;
	}
//--------------------------------------------------
	$meta_boxes[] = array(
		'title'  => esc_html__( 'StarFall Option', 'hemelios' ),
		'id'     => $prefix . 'starfall_metabox',
		'pages'  => array( 'page' ),
		'format' => 'post-format',
		'fields' => array(
			array(
				'name'        => esc_html__( 'Trình độ', 'hemelios' ),
				'id'          => $prefix . 'page_level',
				'type'        => 'select_advanced',
				'options'     => $level_list,
				'placeholder' => esc_html__( 'Chọn trình độ', 'hemelios' ),
				'std'         => '',
				'multiple'    => false,
			),
		),
	);
	$meta_boxes[] = array(
		'title'  => esc_html__( 'StarFall Option', 'hemelios' ),
		'id'     => $prefix . 'starfall_check_level_metabox',
		'pages'  => array( 'page' ),
		'format' => 'post-format',
		'fields' => array(

			array(
				'name'        => esc_html__( 'Bài test cho Jump Start', 'hemelios' ),
				'id'          => $prefix . 'jump-start',
				'type'        => 'select_advanced',
				'options'     => $page_list,
				'placeholder' => esc_html__( 'Chọn bài test', 'hemelios' ),
				'std'         => '',
				'multiple'    => false,
			),
			array(
				'name'        => esc_html__( 'Bài test cho Starters', 'hemelios' ),
				'id'          => $prefix . 'starters',
				'type'        => 'select_advanced',
				'options'     => $page_list,
				'placeholder' => esc_html__( 'Chọn bài test', 'hemelios' ),
				'std'         => '',
				'multiple'    => false,
			),
			array(
				'name'        => esc_html__( 'Bài test cho Movers', 'hemelios' ),
				'id'          => $prefix . 'movers',
				'type'        => 'select_advanced',
				'options'     => $page_list,
				'placeholder' => esc_html__( 'Chọn bài test', 'hemelios' ),
				'std'         => '',
				'multiple'    => false,
			),
			array(
				'name'        => esc_html__( 'Bài test cho Flyers', 'hemelios' ),
				'id'          => $prefix . 'flyers',
				'type'        => 'select_advanced',
				'options'     => $page_list,
				'placeholder' => esc_html__( 'Chọn bài test', 'hemelios' ),
				'std'         => '',
				'multiple'    => false,
			),
		),
	);

	if ( class_exists( 'RW_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}
add_action( 'admin_init', 'starfall_register_meta_boxes' );





if ( !function_exists( 'test_register_action_callback' ) ) {
	function test_register_action_callback() {
		$name_child = $_REQUEST['name_child'];
		$name_date = $_REQUEST['name_date'];
		$join_time = $_REQUEST['join_time'];
		$you_name = $_REQUEST['you_name'];
		$you_phone = $_REQUEST['you_phone'];
		$you_email = $_REQUEST['you_email'];
		$you_message = $_REQUEST['you_message'];

		$html = '';
		$html .= '<p><strong>Họ Tên Trẻ:</strong> '. $name_child .'';
		$html .= '<p><strong>Ngày sinh trẻ:</strong> '. $name_date .'';
		$html .= '<p><strong>Trẻ đã học tiếng anh trong bao lâu:</strong> '. $join_time .'';
		$html .= '<p><strong>Họ tên phụ huynh:</strong> '. $you_name .'';
		$html .= '<p><strong>Số điện thoại:</strong> '. $you_phone .'';
		$html .= '<p><strong>Email:</strong> '. $you_email .'';
		$html .= '<p><strong>Tin nhắn:</strong> '. $you_message .'';

		$post_content = $html;

		$post_id = wp_insert_post( array(
			'post_title'   =>'Trẻ: ' . $name_child . '',
			'post_type'    => 'register_tests',
			'post_status'  => 'pending',
			'post_content' => $post_content
		) );
		if( $post_id ){
			update_post_meta($post_id, 'name_child', $name_child );
			update_post_meta($post_id, 'name_date', $name_date );
			update_post_meta($post_id, 'join_time', $join_time );
			update_post_meta($post_id, 'join_time', $join_time );
			update_post_meta($post_id, 'you_name', $you_name );
			update_post_meta($post_id, 'you_phone', $you_phone );
			update_post_meta($post_id, 'you_email', $you_email );
			update_post_meta($post_id, 'you_message', $you_message );
		}
		die();
	}

	add_action( 'wp_ajax_nopriv_test_register_action', 'test_register_action_callback' );
	add_action( 'wp_ajax_test_register_action', 'test_register_action_callback' );
}

//add_action( 'updated_post_meta', 'wpse16835_after_post_meta', 10, 4 );


if ( !function_exists( 'check_level_action_callback' ) ) {
	function check_level_action_callback() {

		$member_level = $_REQUEST['member_level'];
		$you_name = $_REQUEST['you_name'];
		$you_phone = $_REQUEST['you_phone'];
		$you_email = $_REQUEST['you_email'];
		$child_name = $_REQUEST['child_name'];
		$child_date = $_REQUEST['child_date'];
		$you_message = $_REQUEST['you_message'];

		$html = '';
		$html .= '<p><strong>Họ Tên Trẻ:</strong> '. $child_name .'';
		$html .= '<p><strong>Năm sinh trẻ:</strong> '. $child_date .'';
		$html .= '<p><strong>Họ tên phụ huynh:</strong> '. $you_name .'';
		$html .= '<p><strong>Số điện thoại:</strong> '. $you_phone .'';
		$html .= '<p><strong>Email:</strong> '. $you_email .'';
		$html .= '<p><strong>Khóa học:</strong> '. $member_level .'';
		$html .= '<p><strong>Tin nhắn:</strong> '. $you_message .'';

		$post_content = $html;

		$post_id = wp_insert_post( array(
			'post_title'   =>'Trẻ: ' . $child_name . '',
			'post_type'    => 'check-level',
			'post_status'  => 'pending',
			'post_content' => $post_content
		) );
		if( $post_id ){
			update_post_meta($post_id, 'name_child', $child_name );
			update_post_meta($post_id, 'name_date', $child_date );
			update_post_meta($post_id, 'level_test', $member_level );
			update_post_meta($post_id, 'you_name', $you_name );
			update_post_meta($post_id, 'you_phone', $you_phone );
			update_post_meta($post_id, 'you_email', $you_email );
			update_post_meta($post_id, 'you_message', $you_message );
		}
		echo $post_id;
		die();
	}

	add_action( 'wp_ajax_nopriv_check_level_action', 'check_level_action_callback' );
	add_action( 'wp_ajax_check_level_action', 'check_level_action_callback' );
}

if ( !function_exists( 'test_update_result_action_callback' ) ) {
	function test_update_result_action_callback() {

		$testID = $_REQUEST['testID'];
		$testResult = $_REQUEST['testResult'];
		$testTotal = $_REQUEST['testTotal'];
		$testPoints = $_REQUEST['testPoints'];
		$test_result_html = $testPoints .'/'.$testTotal .' ('.$testResult.'%) ';
		if( $testID ){
			update_post_meta($testID, 'you_score', $test_result_html );
		}
		die();
	}

	add_action( 'wp_ajax_nopriv_test_update_result_action', 'test_update_result_action_callback' );
	add_action( 'wp_ajax_test_update_result_action', 'test_update_result_action_callback' );
}