<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 4/26/2016
 * Time: 10:09 AM
 */

if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( !defined( 'HEMELIOS_REGISTER_TEST_CATEGORY_TAXONOMY' ) ) {
	define( 'HEMELIOS_REGISTER_TEST_CATEGORY_TAXONOMY', 'register_tests-category' );
}

if ( !defined( 'HEMELIOS_REGISTER_TEST_POST_TYPE' ) ) {
	define( 'HEMELIOS_REGISTER_TEST_POST_TYPE', 'register_tests' );
}

if ( !class_exists( 'HemeliosFramework_Register_Test' ) ) {
	class HemeliosFramework_Register_Test {
		function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'front_scripts' ), 11 );
			add_action( 'init', array( $this, 'register_post_types' ), 6 );
			add_filter( 'rwmb_meta_boxes', array( $this, 'register_meta_boxes' ) );
			add_action( 'add_meta_boxes', array( $this, 'add_resgister_fields' ) );

			if ( is_admin() ) {
				add_filter( 'manage_edit-' . HEMELIOS_REGISTER_TEST_POST_TYPE . '_columns', array( $this, 'add_register_tests_columns' ) );
				add_action( 'manage_' . HEMELIOS_REGISTER_TEST_POST_TYPE . '_posts_custom_column', array( $this, 'set_register_tests_columns_value' ), 10, 2 );

			}
//            $this->includes();

		}

		function front_scripts() {
			global $hemelios_options;
		}

		function register_post_types() {

			$post_type = HEMELIOS_REGISTER_TEST_POST_TYPE;

			if ( post_type_exists( $post_type ) ) {
				return;
			}

			$post_type_slug = get_option( 'hemelios-hemelios-' . $post_type . '-config' );
			if ( !isset( $post_type_slug ) || !is_array( $post_type_slug ) ) {
				$slug = 'register-test';
				$name = $singular_name = 'Đăng ký học thử';
			} else {
				$slug          = $post_type_slug['slug'];
				$name          = $post_type_slug['name'];
				$singular_name = $post_type_slug['Đăng ký test'];
			}

			register_post_type( $post_type,
				array(
					'label'       => __( 'Đăng ký học thử', 'hemelios' ),
					'description' => __( 'Đăng ký học thử', 'hemelios' ),
					'labels'      => array(
						'name'               => $name,
						'singular_name'      => $singular_name,
						'menu_name'          => __( $name, 'hemelios' ),
						'parent_item_colon'  => __( 'Parent Item:', 'hemelios' ),
						'all_items'          => __( sprintf( 'All %s', $name ), 'hemelios' ),
						'view_item'          => __( 'View Item', 'hemelios' ),
						'add_new_item'       => __( sprintf( 'Add New  %s', $name ), 'hemelios' ),
						'add_new'            => __( 'Add New', 'hemelios' ),
						'edit_item'          => __( 'Edit Item', 'hemelios' ),
						'update_item'        => __( 'Update Item', 'hemelios' ),
						'search_items'       => __( 'Search Item', 'hemelios' ),
						'not_found'          => __( 'Not found', 'hemelios' ),
						'not_found_in_trash' => __( 'Not found in Trash', 'hemelios' ),
					),
					'supports'    => array( 'title' ),
					'public'      => true,
					'show_ui'     => true,
					'_builtin'    => false,
					'has_archive' => true,
					'menu_icon'   => 'dashicons-edit',
					'rewrite'     => array( 'slug' => $slug, 'with_front' => true ),
				)
			);
			flush_rewrite_rules();

		}

		function add_resgister_fields() {
			add_meta_box( 'show_resgister_field', 'Thông tin đăng ký', array( $this, 'show_resgister_infomation' ), HEMELIOS_REGISTER_TEST_POST_TYPE, 'normal', 'high', array() );
		}

		function show_resgister_infomation( $post ) {
			echo $post->post_content;

		}

		function register_meta_boxes( $meta_boxes ) {
			$meta_boxes[] = array(
				'title'  => __( 'Chỉnh sửa thông tin đăng ký', 'hemelios' ),
				'id'     => 'hemelios-meta-box-register_test-format',
				'pages'  => array( HEMELIOS_REGISTER_TEST_POST_TYPE ),
				'fields' => array(
					array(
						'name' => __( 'Họ tên trẻ', 'hemelios' ),
						'id'   => 'name_child',
						'type' => 'text',
					),
					array(
						'name' => __( 'Ngày sinh trẻ', 'hemelios' ),
						'id'   => 'name_date',
						'type' => 'date',
					),
					array(
						'name' => __( 'Bé đã học tiếng anh trong bao lâu?', 'hemelios' ),
						'id'   => 'join_time',
						'type' => 'text',
					),
					array(
						'name' => __( 'Họ và tên phụ huynh', 'hemelios' ),
						'id'   => 'you_name',
						'type' => 'text',
					),
					array(
						'name' => __( 'Số điện thoại phụ huynh', 'hemelios' ),
						'id'   => 'you_phone',
						'type' => 'text',
					),
					array(
						'name' => __( 'Email phụ huynh', 'hemelios' ),
						'id'   => 'you_email',
						'type' => 'text',
					),
					array(
						'name' => __( 'Tin nhắn', 'hemelios' ),
						'id'   => 'you_message',
						'type' => 'textarea',
					),
				)
			);

			return $meta_boxes;
		}

		function add_register_tests_columns( $columns ) {
			unset(
				$columns['title'],
				$columns['date']
			);
			$cols = array_merge( array( 'cb' => ( '' ) ), $columns );
			$cols = array_merge( $cols, array( 'title' => __( 'Họ Tên Trẻ', 'hemelios' ) ) );
			$cols = array_merge( $cols, array( 'you_name' => __( 'Tên phụ huynh', 'hemelios' ) ) );
			$cols = array_merge( $cols, array( 'you_phone' => __( 'Số điện thoại', 'hemelios' ) ) );

			return $cols;
		}

		function set_register_tests_columns_value( $column, $post_id ) {
			switch ( $column ) {
				case 'id': {
					echo wp_kses_post( $post_id );
					break;
				}
				case 'title' : {
					echo get_post_meta( $post_id, 'name_child', true );
					break;
				}
				case 'you_name' : {
					echo get_post_meta( $post_id, 'you_name', true );
					break;
				}
				case 'you_phone' : {
					echo get_post_meta( $post_id, 'you_phone', true );
					break;
				}

			}
		}

	}

	new HemeliosFramework_Register_Test();
}