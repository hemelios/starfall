<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 4/22/2016
 * Time: 9:51 AM
 */

add_action( 'show_user_profile', 'hemelios_extra_profile_fields' );
add_action( 'edit_user_profile', 'hemelios_extra_profile_fields' );
add_action( 'user_new_form', 'hemelios_extra_profile_fields' );

function hemelios_extra_profile_fields( $user ) { ?>
	<?php
	$hemelios_options = hemelios_option();

//	Level List
	$level_list   = $hemelios_options['level_list'];
	$user_level   = get_the_author_meta( 'member_level', $user->ID );
	$select_level = '';
	$selected     = '';
	if ( $level_list ) :
		$select_level .= '<select class="regular-text" name="member_level">';
		foreach ( $level_list as $value ) {
			$selected = $user_level == sanitize_title( $value ) ? 'selected' : '';
			$select_level .= '<option value="' . sanitize_title( $value ) . '" ' . $selected . '>' . $value . '</option>';
		}
		$select_level .= '</select>';
	endif;


//	Teacher List
	$teacher_list = $hemelios_options['teacher_list'];
	$user_techer  = get_the_author_meta( 'user_techer', $user->ID );

	$select_teacher = '';
	$selected       = '';
	if ( $teacher_list ) :
		$select_teacher .= '<select class="regular-text" name="user_techer">';
		foreach ( $teacher_list as $value ) {
			$selected = $user_techer == sanitize_title( $value ) ? 'selected' : '';
			$select_teacher .= '<option value="' . sanitize_title( $value ) . '" ' . $selected . '>' . $value . '</option>';
		}
		$select_teacher .= '</select>';
	endif;

//	Class List
	$class_list = $hemelios_options['class_list'];
	$user_class = get_the_author_meta( 'user_class', $user->ID );

	$select_class = '';
	$selected     = '';
	if ( $class_list ) :
		$select_class .= '<select class="regular-text" name="user_class">';
		foreach ( $class_list as $value ) {
			$selected = $user_class == sanitize_title( $value ) ? 'selected' : '';
			$select_class .= '<option value="' . sanitize_title( $value ) . '" ' . $selected . '>' . $value . '</option>';
		}
		$select_class .= '</select>';
	endif;

	?>


	<h3>Thông tin học sinh</h3>

	<table class="form-table">
		<tr>
			<th><label for="member_level">Trình độ</label></th>

			<td>
				<?php echo $select_level ?> <br />
				<span class="description">Vui lòng chọn trình độ</span>
			</td>
		</tr>
		<tr>
			<th><label for="user_class">Lóp</label></th>

			<td>
				<?php echo $select_class ?> <br />
				<span class="description">Vui lòng chọn lớp</span>
			</td>
		</tr>
		<tr>
			<th><label for="user_techer">Teacher</label></th>

			<td>
				<?php echo $select_teacher ?> <br />
				<span class="description">Vui lòng chọn giáo viên</span>
			</td>
		</tr>
		<tr>
			<th><label for="expiration_date">Ngày hết hạn tài khoản</label></th>
			<td>
				<input type="text" name="expiration_date" id="expiration_date" value="<?php echo esc_attr( get_the_author_meta( 'expiration_date', $user->ID ) ); ?>" class="regular-text" />
			</td>
		</tr>
		<tr>
			<td>
				<input style="display: none;" type="text" name="notice_status" id="notice_status" value="<?php echo esc_attr( get_the_author_meta( 'notice_status', $user->ID ) ); ?>" class="regular-text" />
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
add_action( 'user_register', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}

	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_user_meta( $user_id, 'member_level', $_POST['member_level'] );
	update_user_meta( $user_id, 'user_techer', $_POST['user_techer'] );
	update_user_meta( $user_id, 'user_class', $_POST['user_class'] );
	update_user_meta( $user_id, 'expiration_date', $_POST['expiration_date'] );
	update_user_meta( $user_id, 'notice_status', '' );
}

if ( !function_exists( 'admin_enqueue_user' ) ) {
	function admin_enqueue_user() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_style( 'jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );
	}

	add_action( 'admin_enqueue_scripts', 'admin_enqueue_user' );
}


//Manager user column\
add_filter( 'manage_users_columns', 'viettitan_add_user_id_column' );
function viettitan_add_user_id_column( $columns ) {
	$columns['level']           = 'Trình Độ';
	$columns['expiration_date'] = 'Ngày hết hạn';
	unset( $columns['posts'] );
	unset( $columns['role'] );

	return $columns;
}

add_action( 'manage_users_custom_column', 'viettitan_show_user_id_column_content', 10, 3 );
function viettitan_show_user_id_column_content( $value, $column_name, $user_id ) {
	$user = get_userdata( $user_id );
	if ( 'level' == $column_name ) {
		return get_the_author_meta( 'member_level', $user_id );
	} elseif ( 'expiration_date' == $column_name ) {
		return get_the_author_meta( 'expiration_date', $user_id );
	}

	return $value;
}

//Add user search column
function add_member_level_filter() {
	$hemelios_options = hemelios_option();
	$level_list       = $hemelios_options['level_list'];
	if ( isset( $_GET['member_level'] ) ) {
		$section = $_GET['member_level'];
		$section = !empty( $section[0] ) ? $section[0] : $section[1];
	} else {
		$section = - 1;
	}
	echo ' <select name="member_level[]" style="float:none;"><option value="">Chọn trình độ</option>';
	foreach ( $level_list as $value ) {
		$selected = sanitize_title( $value ) == $section ? 'selected' : '';
		echo '<option value="' . sanitize_title( $value ) . '"' . $selected . '>' . $value . '</option>';
	}

	echo '<input type="submit" class="button" value="Filter">';
}

add_action( 'restrict_manage_users', 'add_member_level_filter' );

function filter_users_by_member_level( $query ) {
	global $pagenow;

	if ( is_admin() && 'users.php' == $pagenow && isset( $_GET['member_level'] ) && is_array( $_GET['member_level'] ) ) {
		$section    = $_GET['member_level'];
		$section    = !empty( $section[0] ) ? $section[0] : $section[1];
		$meta_query = array(
			array(
				'key'   => 'member_level',
				'value' => $section
			)
		);
		$query->set( 'meta_key', 'member_level' );
		$query->set( 'meta_query', $meta_query );
	}
}

add_filter( 'pre_get_users', 'filter_users_by_member_level' );


if ( !function_exists( 'viettitan_subdate' ) ) :
	function viettitan_subdate( $date1, $date2 ) {
		$date1 = explode( "-", $date1 );
		$date2 = explode( "-", $date2 );
		$date2 = mktime( 0, 0, 0, $date2[1], $date2[0], $date2[2] );
		$date1 = mktime( 0, 0, 0, $date1[1], $date1[0], $date1[2] );
		$d     = $date2 - $date1;
		$days  = abs( floor( $d / ( 365 * 24 * 10 ) ) );

		return $days;
	}
endif;

//Lấy giá trị ngày hết hạn còn lại

if ( !function_exists( 'viettitan_get_expiration_date' ) ) :
	function viettitan_get_expiration_date( $userID ) {
		$expiration_date = get_the_author_meta( 'expiration_date', $userID );
		$now             = date( 'd-m-Y' );

		return viettitan_subdate( $now, $expiration_date );
	}
endif;

if ( !function_exists( 'user_remove_admin_bar' ) ) {
	function user_remove_admin_bar() {
		$userID          = get_current_user_id();
		if ( ! is_super_admin( $userID ) ) {
			show_admin_bar( false );
		}
	}

	add_action( 'init', 'user_remove_admin_bar', 1 );
}


if ( !function_exists( 'user_get_limit_date' ) ) {
	function user_get_limit_date() {
		if ( is_user_logged_in() ) {
			$userID          = get_current_user_id();
			$expiration_date = get_the_author_meta( 'expiration_date', $userID );
			$now             = date( 'd-m-Y' );
			if ( $expiration_date != '' && strtotime( $now ) <= strtotime( $expiration_date ) ) :
				$limit_date = viettitan_subdate( $expiration_date, $now );
				if ( $limit_date <= 7 && $limit_date > 0 ) {
					$notice_status = get_the_author_meta( 'notice_status', $userID );
					if ( $notice_status == '' ) :
						?>
						<div class="box-notice">
							<div class="notice-inner">
								<div class="container">
									<p>Tài khoản của bạn sẽ hết hạn vào ngày <span><?php echo $expiration_date ?></span>, hãy liên hệ với quản trị viên để biết thêm chi tiết</p>
								</div>
								<a class="turn-off-notice" href="javascript:;" data-id="<?php echo $userID ?>">Tắt thông báo</a>
							</div>
						</div>
						<?php
					endif;
				}
			endif;
		}
	}

	add_action( 'wp_footer', 'user_get_limit_date' );
}
if ( !function_exists( 'user_add_body_class' ) ) {
	function user_add_body_class( $classes ) {
		if ( is_user_logged_in() ) {
			$userID          = get_current_user_id();
			$expiration_date = get_the_author_meta( 'expiration_date', $userID );
			$now             = date( 'd-m-Y' );
			if ( $expiration_date != '' && strtotime( $now ) <= strtotime( $expiration_date ) ) :
				$limit_date = viettitan_subdate( $expiration_date, $now );
				if ( $limit_date <= 7 && $limit_date > 0 ) {
					$notice_status = get_the_author_meta( 'notice_status', $userID );
					if ( $notice_status == '' ) :
						$classes[] = 'user-limit-date';
					endif;
				}
			endif;

			return $classes;
		}
	}

	add_filter( 'body_class', 'user_add_body_class' );
}

if ( !function_exists( 'hemelios_user_remove_notice_callback' ) ) {
	function hemelios_user_remove_notice_callback() {
		$userID = $_REQUEST['id'];
		update_user_meta( $userID, 'notice_status', '1' );
		die();
	}

	add_action( 'wp_ajax_nopriv_user_remove_notice', 'hemelios_user_remove_notice_callback' );
	add_action( 'wp_ajax_user_remove_notice', 'hemelios_user_remove_notice_callback' );
}
