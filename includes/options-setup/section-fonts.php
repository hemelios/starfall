<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'icon'   => 'el-icon-fontsize',
		'title'  => esc_html__( 'Fonts', 'hemelios' ),
		'desc'   => esc_html__( 'If you change value in this section, you must "Save & Generate CSS"', 'hemelios' ),
		'fields' => array(
			array(
				'id'          => 'primary_font',
				'type'        => 'typography',
				'title'       => esc_html__( 'Primary Font', 'hemelios' ),
				'subtitle'    => esc_html__( 'Specify the primary font properties.', 'hemelios' ),
				'google'      => true,
				'all_styles'  => true, // Enable all Google Font style/weight variations to be added to the page
				'line-height' => false,
				'color'       => false,
				'text-align'  => false,
				'font-style'  => false,
				'subsets'     => false,
				'font-size'   => false,
				'font-weight' => false,
				'output'      => array( '' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'    => array( '' ), // An array of CSS selectors to apply this font style to dynamically
				'units'       => 'px', // Defaults to px
				'default'     => array(
					'font-family' => 'Roboto',
				),
			),

			array(
				'id'          => 'secondary_font',
				'type'        => 'typography',
				'title'       => esc_html__( 'Secondary Font', 'hemelios' ),
				'subtitle'    => esc_html__( 'Specify the secondary font properties.', 'hemelios' ),
				'google'      => true,
				'all_styles'  => true, // Enable all Google Font style/weight variations to be added to the page
				'line-height' => false,
				'color'       => false,
				'text-align'  => false,
				'font-style'  => false,
				'subsets'     => false,
				'font-size'   => false,
				'font-weight' => false,
				'output'      => array( '' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'    => array( '' ), // An array of CSS selectors to apply this font style to dynamically
				'units'       => 'px', // Defaults to px
				'default'     => array(
					'font-family' => 'Roboto',
				),
			),

			array(
				'id'             => 'body_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'Body Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the body font properties.', 'hemelios' ),
				'google'         => true,
				'text-align'     => false,
				'color'          => false,
				'letter-spacing' => false,
				'line-height'    => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'body' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'body' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '13px',
					'font-family' => 'Roboto',
					'font-weight' => '300',
				),
			),
			array(
				'id'             => 'h1_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'H1 Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the H1 font properties.', 'hemelios' ),
				'google'         => true,
				'text-align'     => false,
				'line-height'    => false,
				'color'          => false,
				'letter-spacing' => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'h1' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'h1' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '32px',
					'line-height' => '48px',
					'font-family' => 'Roboto',
					'font-weight' => '500',
				),
			),
			array(
				'id'             => 'h2_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'H2 Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the H2 font properties.', 'hemelios' ),
				'google'         => true,
				'line-height'    => false,
				'text-align'     => false,
				'color'          => false,
				'letter-spacing' => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'h2' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'h2' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '24px',
					'line-height' => '36px',
					'font-family' => 'Roboto',
					'font-weight' => '500',
				),
			),
			array(
				'id'             => 'h3_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'H3 Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the H3 font properties.', 'hemelios' ),
				'google'         => true,
				'text-align'     => false,
				'line-height'    => false,
				'color'          => false,
				'letter-spacing' => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'h3' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'h3' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '22px',
					'line-height' => '28px',
					'font-family' => 'Roboto',
					'font-weight' => '500',
				),
			),
			array(
				'id'             => 'h4_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'H4 Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the H4 font properties.', 'hemelios' ),
				'google'         => true,
				'text-align'     => false,
				'line-height'    => false,
				'color'          => false,
				'letter-spacing' => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'h4' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'h4' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '18px',
					'line-height' => '24px',
					'font-family' => 'Roboto',
					'font-weight' => '500',
				),
			),
			array(
				'id'             => 'h5_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'H5 Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the H5 font properties.', 'hemelios' ),
				'google'         => true,
				'line-height'    => false,
				'text-align'     => false,
				'color'          => false,
				'letter-spacing' => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'h5' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'h5' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '16px',
					'line-height' => '22px',
					'font-family' => 'Roboto',
					'font-weight' => '500',
				),
			),
			array(
				'id'             => 'h6_font',
				'type'           => 'typography',
				'title'          => esc_html__( 'H6 Font', 'hemelios' ),
				'subtitle'       => esc_html__( 'Specify the H6 font properties.', 'hemelios' ),
				'google'         => true,
				'line-height'    => false,
				'text-align'     => false,
				'color'          => false,
				'letter-spacing' => false,
				'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
				'output'         => array( 'h6' ), // An array of CSS selectors to apply this font style to dynamically
				'compiler'       => array( 'h6' ), // An array of CSS selectors to apply this font style to dynamically
				'units'          => 'px', // Defaults to px
				'default'        => array(
					'font-size'   => '12px',
					'line-height' => '16px',
					'font-family' => 'Roboto',
					'font-weight' => '500',
				),
			)

		),
	) );