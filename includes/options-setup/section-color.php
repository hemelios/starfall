<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'General Colors', 'hemelios' ),
		'desc'   => esc_html__( 'If you change value in this section, you must "Save & Generate CSS"', 'hemelios' ),
		'icon'   => 'el el-magic',
		'fields' => array(
			array(
				'id'       => 'primary_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Primary Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set primary color.', 'hemelios' ),
				'default'  => '#0cb4ce',
				'validate' => 'color',
			),

			array(
				'id'       => 'secondary_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Secondary Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set secondary color.', 'hemelios' ),
				'default'  => '#222222',
				'validate' => 'color',
			),
			array(
				'id'       => 'text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set text color.', 'hemelios' ),
				'default'  => '#444444',
				'validate' => 'color',
			),
			array(
				'id'       => 'text_secondary_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Text Secondary Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set text secondary color.', 'hemelios' ),
				'default'  => '#666',
				'validate' => 'color',
			),

			array(
				'id'       => 'meta_text_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Meta Text Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set meta text color.', 'hemelios' ),
				'default'  => '#999999',
				'validate' => 'color',
			),

			array(
				'id'       => 'bold_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Bolder Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set bolder color.', 'hemelios' ),
				'default'  => '#222222',
				'validate' => 'color',
			),

			array(
				'id'       => 'border_color',
				'type'     => 'color_rgba',
				'title'    => esc_html__( 'Border color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set border color.', 'hemelios' ),
				'default'  => array(
					'color' => '#eee',
					'alpha' => '0.5'
				),
				'mode'     => 'background',
				'validate' => 'colorrgba',
			),

			array(
				'id'       => 'heading_color',
				'type'     => 'color',
				'title'    => esc_html__( 'Heading Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Set heading color.', 'hemelios' ),
				'default'  => '#222222',
				'validate' => 'color',
			),


			array(
				'id'       => 'link_color',
				'type'     => 'link_color',
				'title'    => esc_html__( 'Link Color', 'hemelios' ),
				'subtitle' => esc_html__( 'Link color.', 'hemelios' ),
				'default'  => array(
					'regular' => '#0cb4ce', // blue
					'hover'   => '#0cb4ce', // red
					'active'  => '#0cb4ce',  // purple
				),
			),
		)
	) );