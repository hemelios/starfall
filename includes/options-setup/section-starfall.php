<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Starfall Config', 'hemelios' ),
		'icon'   => 'fa fa-cogs',
		'fields' => array(
			//Header Color
			array(
				'id'     => 'section-starfall-layout',
				'type'   => 'section',
				'title'  => esc_html__( 'Tài khoản Starfall.com', 'hemelios' ),
				'indent' => true
			),
			array(
				'id'       => 'starfall_account',
				'type'     => 'text',
				'title'    => __( 'Tên tài khoản Starfall.com', 'hemelios' ),
				'subtitle' => __( 'Nhập tên tài khoản starfall.com', 'hemelios' ),
			),
			array(
				'id'       => 'starfall_password',
				'type'     => 'text',
				'title'    => __( 'Mật khẩu tài khoản Starfall.com', 'hemelios' ),
				'subtitle' => __( 'Nhập Mật khẩu tài khoản starfall.com', 'hemelios' ),
			),
			array(
				'id'   => 'starfall_divide',
				'type' => 'divide'
			),
			array(
				'id'       => 'level_list',
				'type'     => 'multi_text',
				'title'    => __( 'Danh sách trình độ', 'hemelios' ),
				'subtitle' => __( 'Nhập danh sách trình độ', 'hemelios' ),
				'add_text'	=> 'Thêm trình độ',
			),
			array(
				'id'       => 'class_list',
				'type'     => 'multi_text',
				'title'    => __( 'Danh sách lớp', 'hemelios' ),
				'subtitle' => __( 'Nhập danh sách lớp', 'hemelios' ),
				'add_text'	=> 'Thêm lớp',
			),
			array(
				'id'       => 'teacher_list',
				'type'     => 'multi_text',
				'title'    => __( 'Danh sách giáo viên', 'hemelios' ),
				'subtitle' => __( 'Nhập danh sách giáo viên', 'hemelios' ),
				'add_text'	=> 'Thêm giáo viên',
			),
		)
	) );