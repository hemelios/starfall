<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Performance', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-dashboard',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'enable_minifile_js',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Use compressed JS file', 'hemelios' ),
				'subtitle' => esc_html__( 'Using or not using compressed js file.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '0'
			),
			array(
				'id'       => 'enable_minifile_css',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Use compressed CSS file', 'hemelios' ),
				'subtitle' => esc_html__( 'Using or not using compressed css file. Remember to regenerate css if you decide to get this option.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '0'
			),
		)
	) );