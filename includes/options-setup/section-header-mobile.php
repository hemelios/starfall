<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Mobile Header', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-th-list',
		'fields' => array(
			array(
				'id'       => 'mobile_header_layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Header Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select header mobile layout', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'header-mobile-1' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/mobile-header-1.PNG' ),
					'header-mobile-2' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/mobile-header-2.PNG' ),
					'header-mobile-3' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/mobile-header-3.PNG' ),

				),
				'default'  => 'header-mobile-1'
			),

			array(
				'id'       => 'mobile_header_menu_drop',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Menu Drop Type', 'hemelios' ),
				'subtitle' => esc_html__( 'Set menu drop type for mobile header', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'dropdown' => esc_html__( 'Dropdown Menu', 'hemelios' ),
					'fly'      => esc_html__( 'Fly Menu', 'hemelios' )
				),
				'default'  => 'fly'
			),

			array(
				'id'       => 'mobile_header_logo',
				'type'     => 'media',
				'url'      => true,
				'title'    => esc_html__( 'Mobile Logo', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload your logo here.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => get_template_directory_uri() . '/assets/images/theme-options/logo.png'
				)
			),

			array(
				'id'      => 'logo_mobile_max_height',
				'type'    => 'dimensions',
				'title'   => esc_html__( 'Logo Mobile Max Height', 'hemelios' ),
				'desc'    => esc_html__( 'You can set a max height for the logo mobile here', 'hemelios' ),
				'units'   => 'px',
				'width'   => false,
				'default' => array(
					'height' => ''
				)
			),

			array(
				'id'      => 'logo_mobile_padding',
				'type'    => 'dimensions',
				'title'   => esc_html__( 'Logo Top/Bottom Padding', 'hemelios' ),
				'desc'    => esc_html__( 'If you would like to override the default logo top/bottom padding, then you can do so here', 'hemelios' ),
				'units'   => 'px',
				'width'   => false,
				'default' => array(
					'height' => ''
				)
			),

			array(
				'id'       => 'mobile_header_top_bar',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Top Bar', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable top bar.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '0'
			),
			array(
				'id'       => 'mobile_header_stick',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Stick Mobile Header', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable stick mobile header.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'mobile_header_search_box',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Search Box', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable search box.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'mobile_header_shopping_cart',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Shopping Cart', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable shopping cart', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
		)
	) );