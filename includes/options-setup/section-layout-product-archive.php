<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */


$archive_shop_title_bg_url = get_template_directory_uri() . '/assets/images/bg-shop-title.jpg';

global $osOpt;

$osOpt->addSection(
	array(
		'title'      => esc_html__( 'Product Archive Options', 'hemelios' ),
		'desc'       => '',
		'icon'       => 'el el-shopping-cart',
		'subsection' => true,
		'fields'     => array(
			array(
				'id'       => 'archive_product_layout',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Archive Product Layout', 'hemelios' ),
				'subtitle' => esc_html__( 'Select archive product layout.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid' ),
				'default'  => 'container'
			),
			array(
				'id'       => 'archive_product_sidebar',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Archive Product Sidebar', 'hemelios' ),
				'subtitle' => esc_html__( 'Set archive product sidebar.', 'hemelios' ),
				'desc'     => '',
				'options'  => array(
					'none'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-none.png' ),
					'left'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-left.png' ),
					'right' => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-right.png' ),
					'both'  => array( 'title' => '', 'img' => get_template_directory_uri() . '/assets/images/theme-options/sidebar-both.png' ),
				),
				'default'  => 'right'
			),
			array(
				'id'       => 'archive_product_sidebar_width',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Sidebar Width', 'hemelios' ),
				'subtitle' => esc_html__( 'Set sidebar width.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( 'small' => 'Small (1/4)', 'large' => 'Large (1/3)' ),
				'default'  => 'small',
				'required' => array( 'archive_product_sidebar', '=', array( 'left', 'both', 'right' ) ),
			),
			array(
				'id'       => 'archive_product_left_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Archive Product Left Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default archive product left sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'woocommerce',
				'required' => array( 'archive_product_sidebar', '=', array( 'left', 'both' ) ),
			),
			array(
				'id'       => 'archive_product_right_sidebar',
				'type'     => 'select',
				'title'    => esc_html__( 'Archive Product Right Sidebar', 'hemelios' ),
				'subtitle' => "Choose the default archive product right sidebar.",
				'data'     => 'sidebars',
				'desc'     => '',
				'default'  => 'woocommerce',
				'required' => array( 'archive_product_sidebar', '=', array( 'right', 'both' ) ),
			),
			array(
				'id'       => 'show_archive_product_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Show Archive Title', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable archive product title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'default'  => '1'
			),
			array(
				'id'       => 'archive_product_title_height',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Archive Product Title Height', 'hemelios' ),
				'desc'     => esc_html__( 'You can set a height for the archive product title here.', 'hemelios' ),
				'required' => array( 'show_archive_product_title', '=', array( '1' ) ),
				'units'    => 'px',
				'width'    => false,
				'default'  => array(
					'height' => '420'
				)
			),

			array(
				'id'       => 'breadcrumbs_in_archive_product_title',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Archive Product Breadcrumbs', 'hemelios' ),
				'subtitle' => esc_html__( 'Enable or disable breadcrumbs in archive product title.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '1' => 'On', '0' => 'Off' ),
				'required' => array( 'show_archive_product_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'archive_product_breadcrumbs_position',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Archive Product Breadcrumbs Positions', 'hemelios' ),
				'subtitle' => esc_html__( 'Select archive product breadcrumbs positions.', 'hemelios' ),
				'desc'     => '',
				'options'  => array( '0' => 'Left', '1' => 'Center', '2' => 'Right' ),
				'required' => array( 'breadcrumbs_in_archive_product_title', '=', array( '1' ) ),
				'default'  => '1'
			),

			array(
				'id'       => 'archive_product_title_bg_image',
				'type'     => 'media',
				'url'      => false,
				'title'    => esc_html__( 'Archive Product Title Background', 'hemelios' ),
				'subtitle' => esc_html__( 'Upload archive product title background.', 'hemelios' ),
				'desc'     => '',
				'default'  => array(
					'url' => $archive_shop_title_bg_url
				)
			)
		)
	) );