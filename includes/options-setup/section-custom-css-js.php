<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Custom CSS & Script', 'hemelios' ),
		'desc'   => esc_html__( 'If you change Custom CSS, you must "Save & Generate CSS"', 'hemelios' ),
		'icon'   => 'el el-edit',
		'fields' => array(
			array(
				'id'       => 'custom_css',
				'type'     => 'ace_editor',
				'mode'     => 'css',
				'theme'    => 'monokai',
				'title'    => esc_html__( 'Custom CSS', 'hemelios' ),
				'subtitle' => esc_html__( 'Add some css to your theme by adding it to this textarea. please do not include any style tags.', 'hemelios' ),
				'desc'     => '',
				'default'  => '',
				'options'  => array( 'minLines' => 20, 'maxLines' => 60 )
			),
			array(
				'id'       => 'custom_js',
				'type'     => 'ace_editor',
				'mode'     => 'javascript',
				'theme'    => 'chrome',
				'title'    => esc_html__( 'Custom JS', 'hemelios' ),
				'subtitle' => esc_html__( 'Add some custom javascript to your theme by adding it to this textarea. please do not include any script tags.', 'hemelios' ),
				'desc'     => '',
				'default'  => '',
				'options'  => array( 'minLines' => 20, 'maxLines' => 60 )
			),

		)
	) );