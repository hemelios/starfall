<?php
/**
 * Created by PhpStorm.
 * User: Sinzii
 * Date: 1/19/2016
 * Time: 11:29 AM
 */

global $osOpt;

$osOpt->addSection(
	array(
		'title'  => esc_html__( 'Social Profiles', 'hemelios' ),
		'desc'   => '',
		'icon'   => 'el el-path',
		'fields' => array(
			array(
				'id'       => 'twitter_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Twitter', 'hemelios' ),
				'subtitle' => "Your Twitter.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'facebook_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Facebook', 'hemelios' ),
				'subtitle' => "Your facebook page/profile url.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'dribbble_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Dribbble', 'hemelios' ),
				'subtitle' => "Your Dribbble.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'vimeo_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Vimeo', 'hemelios' ),
				'subtitle' => "Your Vimeo.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'tumblr_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Tumblr', 'hemelios' ),
				'subtitle' => "Your Tumblr.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'skype_username',
				'type'     => 'text',
				'title'    => esc_html__( 'Skype', 'hemelios' ),
				'subtitle' => "Your Skype username.",
				'desc'     => 'Your Skype username.',
				'default'  => ''
			),
			array(
				'id'       => 'linkedin_url',
				'type'     => 'text',
				'title'    => esc_html__( 'LinkedIn', 'hemelios' ),
				'subtitle' => "Your LinkedIn page/profile url.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'googleplus_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Google+', 'hemelios' ),
				'subtitle' => "Your Google+ page/profile URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'flickr_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Flickr', 'hemelios' ),
				'subtitle' => "Your Flickr page url.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'youtube_url',
				'type'     => 'text',
				'title'    => esc_html__( 'YouTube', 'hemelios' ),
				'subtitle' => "Your YouTube URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'pinterest_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Pinterest', 'hemelios' ),
				'subtitle' => "Your Pinterest.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'foursquare_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Foursquare', 'hemelios' ),
				'subtitle' => "Your Foursqaure URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'instagram_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Instagram', 'hemelios' ),
				'subtitle' => "Your Instagram.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'github_url',
				'type'     => 'text',
				'title'    => esc_html__( 'GitHub', 'hemelios' ),
				'subtitle' => "Your GitHub URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'xing_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Xing', 'hemelios' ),
				'subtitle' => "Your Xing URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'behance_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Behance', 'hemelios' ),
				'subtitle' => "Your Behance URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'deviantart_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Deviantart', 'hemelios' ),
				'subtitle' => "Your Deviantart URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'soundcloud_url',
				'type'     => 'text',
				'title'    => esc_html__( 'SoundCloud', 'hemelios' ),
				'subtitle' => "Your SoundCloud URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'yelp_url',
				'type'     => 'text',
				'title'    => esc_html__( 'Yelp', 'hemelios' ),
				'subtitle' => "Your Yelp URL.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'rss_url',
				'type'     => 'text',
				'title'    => esc_html__( 'RSS Feed', 'hemelios' ),
				'subtitle' => "Your RSS Feed URL",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'       => 'email_address',
				'type'     => 'text',
				'title'    => esc_html__( 'Email address', 'hemelios' ),
				'subtitle' => "Your email address.",
				'desc'     => '',
				'default'  => ''
			),
			array(
				'id'   => 'social-profile-divide-0',
				'type' => 'divide'
			),
			array(
				'title'    => esc_html__( 'Social Share', 'hemelios' ),
				'id'       => 'social_sharing',
				'type'     => 'checkbox',
				'subtitle' => esc_html__( 'Show the social sharing in blog posts.', 'hemelios' ),

				//Must provide key => value pairs for multi checkbox options
				'options'  => array(
					'facebook'  => 'Facebook',
					'twitter'   => 'Twitter',
					'google'    => 'Google',
					'linkedin'  => 'Linkedin',
					'tumblr'    => 'Tumblr',
					'pinterest' => 'Pinterest'
				),

				//See how default has changed? you also don't need to specify opts that are 0.
				'default'  => array(
					'facebook'  => '1',
					'twitter'   => '1',
					'google'    => '1',
					'linkedin'  => '1',
					'tumblr'    => '1',
					'pinterest' => '1'
				)
			),
			array(
				'title'    => esc_html__( 'Social share on page 404', 'hemelios' ),
				'id'       => 'social_sharing_404',
				'type'     => 'checkbox',
				'subtitle' => esc_html__( 'Show the social sharing in page 404.', 'hemelios' ),

				//Must provide key => value pairs for multi checkbox options
				'options'  => array(
					'facebook' => 'Facebook',
					'twitter'  => 'Twitter',
					'google'   => 'Google',
					'behance'  => 'Behance',
					'skype'    => 'Skype'
				),

				//See how default has changed? you also don't need to specify opts that are 0.
				'default'  => array(
					'facebook' => '1',
					'twitter'  => '1',
					'google'   => '1',
					'behance'  => '1',
					'skype'    => '1'
				)
			)
		)
	) );