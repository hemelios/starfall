<?php
/**
 * Created by PhpStorm.
 * User: PhanLong
 * Date: 4/9/2016
 * Time: 12:41 PM
 */

//Customs Viettitan RSS Feed
if ( !class_exists( 'viettitanDashboardWidget' ) ) {
	class viettitanDashboardWidget {
		public function __construct() {
			add_action( 'wp_dashboard_setup', array( $this, 'add_viettitan_dashboard' ) );
		}

		public function add_viettitan_dashboard() {
			add_meta_box( 'viettitan_dashboard_widget', 'Viettitan Update', array( $this, 'viettitan_dashboard_widget' ), 'dashboard', 'side', 'high' );
		}

		public function viettitan_dashboard_widget() {
			echo '<div class="rss-widget">';
			wp_widget_rss_output( array(
				'url'          => 'http://viettitan.com/feed/',
				'title'        => 'VIETTITAN_NEWS',
				'items'        => 4,
				'show_summary' => 1,
				'show_author'  => 0,
				'show_date'    => 1
			) );
			echo '</div>';
		}
	}

	new viettitanDashboardWidget();
}

// remove unwanted dashboard widgets for relevant users
function wptutsplus_remove_dashboard_widgets() {
	$user = wp_get_current_user();
	if ( !$user->has_cap( 'manage_options' ) ) {
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_activity', 'dashboard', 'side' );
	}
}

add_action( 'wp_dashboard_setup', 'wptutsplus_remove_dashboard_widgets' );

// Move the 'Right Now' dashboard widget to the right hand side
function wptutsplus_move_dashboard_widget() {
	$user = wp_get_current_user();
	if ( !$user->has_cap( 'manage_options' ) ) {
		global $wp_meta_boxes;
		$widget = $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'];
		unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
		$wp_meta_boxes['dashboard']['side']['core']['dashboard_right_now'] = $widget;
	}
}

add_action( 'wp_dashboard_setup', 'wptutsplus_move_dashboard_widget' );

// add new dashboard widgets
function wptutsplus_add_dashboard_widgets() {
	wp_add_dashboard_widget( 'wptutsplus_dashboard_welcome', 'Welcome', 'wptutsplus_add_welcome_widget' );
}


function wptutsplus_add_welcome_widget() { ?>
	<h3>Thông báo</h3>
	<ul class="list-notice">
		<?php
		$args      = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => '',
			'meta_key'     => '',
			'meta_value'   => '',
			'meta_compare' => '',
			'meta_query'   => array(),
			'date_query'   => array(),
			'include'      => array(),
			'exclude'      => array(),
			'orderby'      => 'login',
			'order'        => 'ASC',
			'offset'       => '',
			'search'       => '',
			'number'       => '',
			'count_total'  => false,
			'fields'       => 'all',
			'who'          => ''
		);
		$list_user = get_users( $args );
		foreach ( $list_user as $value ) {
			$userID          = $value->ID;
			$expiration_date = get_the_author_meta( 'expiration_date', $userID );
			$now             = date( 'd-m-Y' );
			if ( $expiration_date != '' && strtotime( $now ) <= strtotime( $expiration_date ) ) :
				$limit_date = viettitan_subdate( $expiration_date, $now );
				if ( $limit_date <= 7 && $limit_date > 0 ) {
					echo '<li class="limit">Tài khoản <strong>' . $value->user_login . '</strong> sắp hết hạn (Còn ' . $limit_date . ' Ngày ) <a href="' . get_edit_user_link( $userID ) . '"> - Gia hạn ngay </a></li>';
				}
			endif;
		}
		?>
	</ul>
<?php }


add_action( 'wp_dashboard_setup', 'wptutsplus_add_dashboard_widgets' );

function starfall_notice_add_dashboard_widgets() {
	wp_add_dashboard_widget( 'starfall_dashboard_notification', 'Thống kê', 'notices_add_welcome_widget' );
}
function notices_add_welcome_widget() { ?>
	<h3>Thông báo</h3>
	<ul class="list-notice">
		<?php
		$args      = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => '',
			'meta_key'     => '',
			'meta_value'   => '',
			'meta_compare' => '',
			'meta_query'   => array(),
			'date_query'   => array(),
			'include'      => array(),
			'exclude'      => array(),
			'orderby'      => 'login',
			'order'        => 'ASC',
			'offset'       => '',
			'search'       => '',
			'number'       => '',
			'count_total'  => true,
			'fields'       => 'all',
			'who'          => ''
		);
		$list_user = get_users( $args );
		$total = 0;
		$user_active = 0;
		$user_band = 0;
		$user_admin = 0;
		foreach ( $list_user as $value ) {
			$userID          = $value->ID;
			$expiration_date = get_the_author_meta( 'expiration_date', $userID );
			$now             = date( 'd-m-Y' );
			if ( is_super_admin( $userID )) {
				$user_admin ++;
			}
			if ( $expiration_date != '' && strtotime( $now ) <= strtotime( $expiration_date ) ) {
				$user_active ++;
			}else{
				$user_band++;
			}

		}
		?>
		<li><i class="icon-collapse"></i>Tổng số tài khoản: <?php echo count($list_user) ?> </li>
		<li>Tài khoản quản trị: <?php echo $user_admin ?> </li>
		<li>Tài khoản học sinh: <?php echo count($list_user)- $user_admin ?> </li>
		<li>Tài khoản hết hạn: <?php echo $user_band ?> </li>
		<li>Tài khoản đang học: <?php echo $user_active ?> </li>
	</ul>
<?php }

add_action( 'wp_dashboard_setup', 'starfall_notice_add_dashboard_widgets' );

remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );


function remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity'] );

}

add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

if ( !current_user_can( 'manage_options' ) ) {
	add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );
}
